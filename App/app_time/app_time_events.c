#include "inc/app_time_events.h"
#include "settings_app/general/settings_app_time_events.h"
#include "debug/log_modules/log_modules.h"

#include "settings_app/general/settings_app_procedure.h"
#include "settings_core/sys_macros.h"
#include "peripherals_app/init/inc/app_peripheral_init.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if (APP_TEVT_ARRAY_SIZE == 0)

/* module not enable - empty api functions */

void app_time_events_init(void)
{
    log_info_m(log_controller, "No application time events initialized!");         
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_exec_handler(void)
{
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_timer_period_elapsed(void)
{
}

#else

#define ADD_TEVT(tevt_id, _interval_sec, _handler)\
    [tevt_id] = { .interval_sec = _interval_sec, .event_handler = _handler }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct tevt_init_s 
{
    uint32_t        interval_sec;
    time_event_cb   event_handler;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_time_events(time_event_t *tevt_arr, 
                                   time_event_t **ptr_tevt_arr,
                                   struct tevt_init_s *init_arr);

static time_evt_ret_t test_tevt_cb(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static time_event_t time_event_array[APP_TEVT_ARRAY_SIZE] = { 0 };
static time_event_t *time_event_ptr_array[APP_TEVT_ARRAY_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct tevt_init_s init_array[APP_TEVT_ARRAY_SIZE] = 
{
    ADD_TEVT(APP_TEVT_TEST, APP_TEVT_TEST_SEC, test_tevt_cb)
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_init(void)
{
    static_assert(APP_TEVT_ARRAY_SIZE == APP_TEVT_NUM, 
        "APP_TEVT_ARRAY_SIZE != APP_TEVT_MAX");

    initialize_time_events(time_event_array, time_event_ptr_array, init_array);

    time_event_register_events_array(time_event_ptr_array, APP_TEVT_NUM);
    // time_event_initialize_timer();

    log_info_m(log_controller, 
            "Application time_events initalize succesfull!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_exec_handler(void)
{    
    time_event_exec_handler();    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_timer_period_elapsed(void)
{    
    time_event_timer_period_elapsed();    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

time_event_t *app_time_events_get_tevt_ptr(enum app_time_event tevent_id)
{
    if(tevent_id >= APP_TEVT_MIN && tevent_id <= APP_TEVT_MAX)
    {
        return &time_event_array[tevent_id];
    }
    return NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_time_events(time_event_t *tevt_arr, 
                                   time_event_t **ptr_tevt_arr,
                                   struct tevt_init_s *init_arr)
{
    for(int tevt_id = 0; tevt_id < APP_TEVT_NUM; tevt_id++)
    {        
        time_event_t *tevt_ptr = &tevt_arr[tevt_id];

        bool res = time_event_init(tevt_ptr, tevt_id,
                                   init_arr[tevt_id].interval_sec,
                                   init_arr[tevt_id].event_handler);

        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g,
            "App tout array err, tevt id [ %d ] not initialized!", tevt_id);   

        ptr_tevt_arr[tevt_id] = tevt_ptr;
    }
}                                   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static time_evt_ret_t test_tevt_cb(void)
{  
    return TIME_EVT_RET_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif