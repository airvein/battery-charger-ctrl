#include "inc/app_timeouts.h"
#include "settings_app/general/settings_app_timeouts.h"

#include "debug/log_modules/log_modules.h"
#include "settings_core/sys_macros.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if (APP_TOUT_ARRAY_SIZE == 0)

/* module not enable - empty api functions */

void app_timeout_init(void)
{
    log_info_m(log_controller, "No application timeouts initialized!");            
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_timeout_timer_period_elapsed(void)
{    
}

#else

#define ADD_TOUT(tout_id, time) \
    [tout_id] = { .interval_mult = time }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct tout_init_s 
{
    uint32_t interval_mult;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static timeout_t tout_array[APP_TOUT_ARRAY_SIZE] = { 0 };
static timeout_t *tout_ptr_array[APP_TOUT_ARRAY_SIZE] = { 0 };

static struct tout_init_s init_array[APP_TOUT_ARRAY_SIZE] = 
{
    ADD_TOUT(APP_TOUT_CTRL_RESET, APP_TOUT_CTRL_RESET_TIM_SEC),
    ADD_TOUT(APP_TOUT_AC_CTRL, APP_TOUT_AC_CTRL_TIM_SEC),
    ADD_TOUT(APP_TOUT_H_HEATER_CTRL, APP_TOUT_H_HEATER_CTRL_TIM_SEC),
    ADD_TOUT(APP_TOUT_R_HEATER_CTRL, APP_TOUT_R_HEATER_CTRL_TIM_SEC),
    ADD_TOUT(APP_TOUT_FAN_CTRL, APP_TOUT_FAN_CTRL_TIM_SEC),
    ADD_TOUT(APP_TOUT_AIR_COMP_CTRL, APP_TOUT_AIR_COMP_CTRL_TIM_SEC)
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_timeout(timeout_t *tout_arr, timeout_t **ptr_tout_arr,
                               struct tout_init_s *init_array);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_timeout_init(void)
{
    static_assert(APP_TOUT_ARRAY_SIZE == APP_TOUT_NUM, 
        "APP_TOUT_ARRAY_SIZE != APP_TOUT_MAX");

    initialize_timeout(tout_array, tout_ptr_array, init_array);

    timeout_register_timeouts_array(tout_ptr_array, APP_TOUT_NUM);
    timeout_initialize_timer();

    log_info_m(log_controller, 
            "Application timeout initalize succesfull!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_timeout_timer_period_elapsed(TIM_HandleTypeDef *htim)
{
    timeout_timer_period_elapsed(htim);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

timeout_t *app_timeout_get_tout_ptr(enum app_timeout timeout_id)
{
    if(timeout_id >= APP_TOUT_MIN && timeout_id <= APP_TOUT_MAX)
    {
        return &tout_array[timeout_id];
    }
    return NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_timeout(timeout_t *tout_arr, timeout_t **ptr_tout_arr,
                               struct tout_init_s *ptr_init_array)
{
    for(int tout_id = 0; tout_id < APP_TOUT_NUM; tout_id++)
    {
        timeout_t *tout_ptr = &tout_arr[tout_id];

        bool res = timeout_init(tout_ptr, tout_id, 
                                ptr_init_array[tout_id].interval_mult);

        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g,
            "App tout array err, tout id [ %d ] not initialized!", tout_id);
        ptr_tout_arr[tout_id] = tout_ptr;
    }
}                               

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif