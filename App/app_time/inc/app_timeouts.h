#ifndef __APP_TIMEOUTS_H
#define __APP_TIMEOUTS_H

#include "time/timeout/inc/timeout.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum app_timeout
{
    APP_TOUT_MIN,


    __APP_TOUT_LAST,
    APP_TOUT_MAX = __APP_TOUT_LAST - 1,
    APP_TOUT_NUM
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_TOUT_ARRAY_SIZE     0   /*  set 0 to disable module */

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_timeout_init(void);

void app_timeout_timer_period_elapsed(void);

timeout_t *app_timeout_get_tout_ptr(enum app_timeout timeout_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_TIMEOUTS_H */