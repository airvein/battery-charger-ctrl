#ifndef __APP_TIME_EVENTS_H
#define __APP_TIME_EVENTS_H

#include "time/time_event/inc/time_event.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum app_time_event
{
    APP_TEVT_MIN,

    APP_TEVT_TEST = APP_TEVT_MIN,

    __APP_TEVT_LAST,
    APP_TEVT_MAX = __APP_TEVT_LAST - 1,
    APP_TEVT_NUM
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_TEVT_ARRAY_SIZE     1   /*  set 0 to disable module */

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_init(void);

void app_time_events_exec_handler(void);

void app_time_events_timer_period_elapsed(void);

time_event_t *app_time_events_get_tevt_ptr(enum app_time_event tevent_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_TIME_EVENTS_H */