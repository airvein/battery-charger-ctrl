#include "inc/canbus_messages.h"
#include "inc/canbus_messages_id.h"
#include "communication/hardware/can/inc/can_open.h"
#include "settings_core/sys_macros.h"
#include "debug/log_modules/log_modules.h"
#include <string.h>
#include "peripherals_app/init/inc/app_peripheral_init.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ADD_CANBUS_MSG(msg_id, _handler_cb, _response_cb)\
    [msg_id] = \
    {.id = msg_id, .handler_cb = _handler_cb, .response_cb = _response_cb }

#define CANBUS_MSG_ID(msg_id, battery_charger_id)\
    (msg_id + (battery_charger_id * CANBUS_MSG_OFFSET))

#define HIBYTEU16(_u16) ((_u16) >> 8)
#define LOBYTEU16(_u16) ((_u16) & 0x00FF)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define __EMPTY_MSG_HANDLER_CB_BODY \
    {\
        log_debug_g("empty_%s", __func__);\
        return CAN_MSG_HANDLE_DONE;\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define __EMPTY_MSG_RESP_CB_BODY \
    {\
        log_debug_g("empty_%s", __func__);\
        can_msg_remote_resp_data_t resp = { 0 };\
        return resp;\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct msg_init_param
{
    uint32_t id;

    can_msg_handler_cb handler_cb;
    can_msg_remote_resp_cb response_cb;
}; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

union bytes2u16 
{ 
    uint8_t bytes[2]; 
    uint16_t u16; 
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool canbus_send_data(canbus_msg_id_t msg_id, uint8_t * data, 
                             uint8_t data_len);

static can_msg_handle_t ctrl_present_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t ctrl_present_response_cb(void);

static can_msg_handle_t battery_state_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_state_response_cb(void);

static can_msg_handle_t battery_voltage_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_voltage_response_cb(void);

static can_msg_handle_t battery_percentage_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_percentage_response_cb(void);

static can_msg_handle_t battery_cell_0_2_voltage_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_cell_0_2_voltage_response_cb(void);

static can_msg_handle_t battery_cell_3_5_voltage_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_cell_3_5_voltage_response_cb(void);

static can_msg_handle_t battery_uid_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_uid_response_cb(void);

static can_msg_handle_t battery_temp_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t battery_temp_response_cb(void);

static can_msg_handle_t current_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t current_response_cb(void);

static can_msg_handle_t temperature_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t temperature_response_cb(void);

static can_msg_handle_t cmd_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t cmd_response_cb(void);

static can_msg_handle_t cmd_resp_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t cmd_resp_response_cb(void);

static can_msg_handle_t warning_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t warning_response_cb(void);

static can_msg_handle_t error_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t error_response_cb(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_t message_array[CANBUS_MSG_NUM] = { 0 };

struct msg_init_param msg_init_array[CANBUS_MSG_NUM] =
{
    ADD_CANBUS_MSG(CANBUS_MSG_ID_CTRL_PRESENT, 
                   ctrl_present_handler_cb, 
                   ctrl_present_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATTERY_STATE, 
                   battery_state_handler_cb, 
                   battery_state_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATTERY_VOLTAGE, 
                   battery_voltage_handler_cb, 
                   battery_voltage_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATTERY_PERCENTAGE,
                   battery_percentage_handler_cb,
                   battery_percentage_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATT_CELL_0_2_VOLTAGE, 
                   battery_cell_0_2_voltage_handler_cb, 
                   battery_cell_0_2_voltage_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATT_CELL_3_5_VOLTAGE, 
                   battery_cell_3_5_voltage_handler_cb, 
                   battery_cell_3_5_voltage_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATTERY_UID, 
                   battery_uid_handler_cb, 
                   battery_uid_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_BATTERY_TEMP, 
                   battery_temp_handler_cb, 
                   battery_temp_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_CURRENT, 
                   current_handler_cb, 
                   current_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_TEMPERATURE, 
                   temperature_handler_cb, 
                   temperature_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_CMD, 
                   cmd_handler_cb, 
                   cmd_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_CMD_RESP, 
                   cmd_resp_handler_cb, 
                   cmd_resp_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_WARNING, 
                   warning_handler_cb, 
                   warning_response_cb),
    ADD_CANBUS_MSG(CANBUS_MSG_ID_ERROR, 
                   error_handler_cb, 
                   error_response_cb)
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void canbus_messages_init(void)
{
    CAN_HandleTypeDef *hcan = can_get_handle_ptr();
    uint8_t battery_charger_id = 2;

    for(int arr_msg_id = 0; arr_msg_id < CANBUS_MSG_NUM; arr_msg_id++)
    {
        can_msg_t *msg_ptr = &message_array[arr_msg_id];
        uint16_t canbus_msg_id = CANBUS_MSG_ID(msg_init_array[arr_msg_id].id, 
                                               battery_charger_id);

        bool res = can_msg_init(msg_ptr, hcan, canbus_msg_id, 
                                msg_init_array[arr_msg_id].handler_cb,
                                msg_init_array[arr_msg_id].response_cb);

        // log_warning_m(log_controller, 
        // "Initialize canbus msg for sensor [ %d ] msg_id [ %d] array_msg id [ %d ] "
        // "canbus_msg_id [ %d ]", sensor_id, msg_id, ARRAY_MSG_ID(msg_id, sensor_id),
        // canbus_msg_id);

        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "canbus_msg [ msg_id %d / battery_charger_id %d ] init err", 
            arr_msg_id, battery_charger_id);
    }

    log_info_m(log_controller, 
        "CANBUS messages battery_charger_ctrl [ %d ] initialized succesfully!",
        battery_charger_id);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

can_msg_t *canbus_messages_get_array(void)
{
    return message_array;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_ctrl_present_msg(void)
{
    uint8_t data[1] = { 0x01 };
    
    return canbus_send_data(CANBUS_MSG_ID_CTRL_PRESENT, data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_state(uint8_t battery_state)
{
    uint8_t data[1] = { battery_state };
    
    return canbus_send_data(CANBUS_MSG_ID_BATTERY_STATE, data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_voltage(uint16_t voltage)
{
    uint8_t data[2] = { LOBYTEU16(voltage), HIBYTEU16(voltage) };

    return canbus_send_data(CANBUS_MSG_ID_BATTERY_VOLTAGE, data, 2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_percentage(uint8_t percentage)
{
    uint8_t data[2] = { LOBYTEU16(percentage), HIBYTEU16(percentage) };

    return canbus_send_data(CANBUS_MSG_ID_BATTERY_VOLTAGE, data, 2);        
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_cell_0_2_voltage(uint16_t cell_voltage[3])
{
    uint8_t data[6] = 
    {
        LOBYTEU16(cell_voltage[0]), HIBYTEU16(cell_voltage[0]),
        LOBYTEU16(cell_voltage[1]), HIBYTEU16(cell_voltage[1]),
        LOBYTEU16(cell_voltage[2]), HIBYTEU16(cell_voltage[2])
    };

    return canbus_send_data(CANBUS_MSG_ID_BATT_CELL_0_2_VOLTAGE, data, 6);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_cell_3_5_voltage(uint16_t cell_voltage[3])
{
    uint8_t data[6] = 
    {
        LOBYTEU16(cell_voltage[0]), HIBYTEU16(cell_voltage[0]),
        LOBYTEU16(cell_voltage[1]), HIBYTEU16(cell_voltage[1]),
        LOBYTEU16(cell_voltage[2]), HIBYTEU16(cell_voltage[2])
    };

    return canbus_send_data(CANBUS_MSG_ID_BATT_CELL_3_5_VOLTAGE, data, 6);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_uid(uint8_t * uid, uint8_t uid_len)
{
    return canbus_send_data(CANBUS_MSG_ID_BATTERY_UID, uid, uid_len);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_battery_temperature(uint8_t temperature)
{
    uint8_t data[1] = { temperature };

    return canbus_send_data(CANBUS_MSG_ID_BATTERY_TEMP, data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_current(uint16_t current)
{
    uint8_t data[2] = { LOBYTEU16(current), HIBYTEU16(current) };

    return canbus_send_data(CANBUS_MSG_ID_CURRENT, data, 2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_temperature(uint8_t temperature)
{
    uint8_t data[1] = { temperature };

    return canbus_send_data(CANBUS_MSG_ID_TEMPERATURE, data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_cmd_resp(uint8_t resp_code)
{
    uint8_t data[1] = { resp_code };

    return canbus_send_data(CANBUS_MSG_ID_CMD_RESP, data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_warning(uint16_t warning_code)
{
    uint8_t data[2] = { LOBYTEU16(warning_code), HIBYTEU16(warning_code) };

    return canbus_send_data(CANBUS_MSG_ID_WARNING, data, 2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_error(uint16_t error_code)
{
    uint8_t data[2] = { LOBYTEU16(error_code), HIBYTEU16(error_code) };

    return canbus_send_data(CANBUS_MSG_ID_ERROR, data, 2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool canbus_send_data(canbus_msg_id_t msg_id, uint8_t * data, 
                             uint8_t data_len)
{
    return can_msg_send_data(&message_array[msg_id], data, data_len);
}                             

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t ctrl_present_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t ctrl_present_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_state_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_state_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_voltage_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_voltage_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_percentage_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_percentage_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_cell_0_2_voltage_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_cell_0_2_voltage_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_cell_3_5_voltage_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_cell_3_5_voltage_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_uid_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_uid_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t battery_temp_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t battery_temp_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t current_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t current_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t temperature_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t temperature_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t cmd_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t cmd_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t cmd_resp_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t cmd_resp_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t warning_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t warning_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t error_handler_cb(can_msg_t *msg)
{
    __EMPTY_MSG_HANDLER_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t error_response_cb(void)
{
    __EMPTY_MSG_RESP_CB_BODY;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|