#ifndef __CANBUS_MESSAGES_H
#define __CANBUS_MESSAGES_H

#include "communication/protocols/can/inc/can_msg_ctrl.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void canbus_messages_init(void);
can_msg_t *canbus_messages_get_array(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool canbus_send_ctrl_present_msg(void);
bool canbus_send_battery_state(uint8_t battery_state);
bool canbus_send_battery_voltage(uint16_t voltage);
bool canbus_send_battery_percentage(uint8_t percentage);
bool canbus_send_battery_cell_0_2_voltage(uint16_t cell_voltage[3]);
bool canbus_send_battery_cell_3_5_voltage(uint16_t cell_voltage[3]);
bool canbus_send_battery_uid(uint8_t * uid, uint8_t uid_len);
bool canbus_send_battery_temperature(uint8_t temperature);
bool canbus_send_current(uint16_t current);
bool canbus_send_temperature(uint8_t temperature);
bool canbus_send_cmd_resp(uint8_t resp_code);
bool canbus_send_warning(uint16_t warning_code);
bool canbus_send_error(uint16_t error_code);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CANBUS_MESSAGES_H */