#ifndef __CANBUS_MESSAGES_ID_H
#define __CANBUS_MESSAGES_ID_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CANBUS_MSG_OFFSET   0x10

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{   
    CANBUS_MSG_ID_MIN = 0,

    CANBUS_MSG_ID_ERROR = 0,
    CANBUS_MSG_ID_WARNING,

    CANBUS_MSG_ID_CTRL_PRESENT,
    CANBUS_MSG_ID_BATTERY_STATE,
    CANBUS_MSG_ID_BATTERY_VOLTAGE,
    CANBUS_MSG_ID_BATTERY_PERCENTAGE,
    CANBUS_MSG_ID_BATT_CELL_0_2_VOLTAGE,
    CANBUS_MSG_ID_BATT_CELL_3_5_VOLTAGE,
    CANBUS_MSG_ID_BATTERY_UID,
    CANBUS_MSG_ID_BATTERY_TEMP,
    CANBUS_MSG_ID_CURRENT,
    CANBUS_MSG_ID_TEMPERATURE,
    CANBUS_MSG_ID_CMD,
    CANBUS_MSG_ID_CMD_RESP,

    __CANBUS_MSG_ID_LAST,
    CANBUS_MSG_ID_MAX = __CANBUS_MSG_ID_LAST - 1,
    CANBUS_MSG_NUM

} canbus_msg_id_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CANBUS_MESSAGES_ID_H */