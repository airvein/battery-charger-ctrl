#include "inc/canbus.h"
#include "inc/canbus_messages.h"
#include "inc/canbus_messages_id.h"
#include "debug/log_modules/log_modules.h"
#include "communication/hardware/can/inc/can_open.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_ctrl_t canbus_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint16_t msg_added_num = 0;

static can_msg_t *canbus_messages_array[CANBUS_MSG_NUM] = { 0 }; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void add_msg_ptr_to_messages_array(can_msg_t *msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void canbus_init(void)
{
    uint16_t canbus_msg = CANBUS_MSG_NUM;

    canbus_messages_init();
    can_msg_t *charger_messages_ptr = canbus_messages_get_array();

    for(int msg_id = 0; msg_id < canbus_msg; msg_id++)
    {
        add_msg_ptr_to_messages_array(&charger_messages_ptr[msg_id]);
    }

    if(canbus_msg != msg_added_num)
    {
        log_error_m(log_controller, 
                    "Not equal amount of can_msg initalization "
                    "/ timeout array size! [ %s : %s ]",
                    __FILE__, __func__);
        while(1); //err infinite loop
    }
    else
    {
        can_msg_ctrl_init(&canbus_ctrl, canbus_messages_array, 
                          msg_added_num);
        uint8_t ctrl_id = 2;
        can_filters_init(ctrl_id, CANBUS_MSG_OFFSET);
        log_info_m(log_controller, 
                    "Application canbus initialized succesfully!");                
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void canbus_msg_execute_handler(void)
{
    can_msg_ctrl_execute_handler(&canbus_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void canbus_msg_order_handler(can_msg_recv_t recv_msg)
{
    can_msg_ctrl_order_handler(&canbus_ctrl, recv_msg);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void add_msg_ptr_to_messages_array(can_msg_t *msg)
{
    if(msg == NULL)
    {
        log_error_m(log_controller, 
            "Try add NULL ptr can_msg to canbus messages "
            "array with [ %d ] id !", msg_added_num);
        return ;
    }

    canbus_messages_array[msg_added_num++] = msg;   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|