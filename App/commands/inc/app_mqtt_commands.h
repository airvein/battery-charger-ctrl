#ifndef __APP_MQTT_COMMANDS_H
#define __APP_MQTT_COMMANDS_H

#include "control/cmd_procedure/inc/cmd_procedure_ctrl.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum app_mqtt_cmd
{
    APP_MQTT_CMD_MIN = 0,

    __APP_MQTT_CMD_LAST,
    APP_MQTT_CMD_MAX = __APP_MQTT_CMD_LAST - 1,
    APP_MQTT_CMD_NUM
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_MQTT_CMD_ARRAY_SIZE    0

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_init(void);

void app_mqtt_commands_handler(void);

bool app_mqtt_cmd_call(enum app_mqtt_cmd cmd_id, char *mqtt_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_MQTT_COMMANDS_H */