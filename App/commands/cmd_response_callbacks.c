#include "inc/cmd_response_callbacks.h"
#include "debug/log_modules/log_modules.h"
#include <stdio.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/* mqtt_cmd action response */
static char *action_resp_str[3] = { "ack", "refuse", "invalid_value" };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool action_response_build(char *resp_str, action_resp_t action_resp, 
                                  uint32_t order_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
__attribute__((unused))
static bool action_response_build(char *resp_str, action_resp_t action_resp, 
                                  uint32_t order_id)
{
	if(action_resp >= ACTION_ACCEPTED && action_resp <= ACTION_SKIPPED)
	{
		sprintf(resp_str, "%s/%ld", action_resp_str[action_resp-1], 
									    order_id);
        return true;
	}
	else
	{
		log_warning_m(log_controller, "Invalid action_response value "
					  "for action response mqtt publish!");
        return false;
	}	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|