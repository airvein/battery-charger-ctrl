#include "inc/app_mqtt_commands.h"
#include "debug/log_modules/log_modules.h"

#include "procedures/inc/app_procedure_ctrl.h"

#include "settings_app/general/settings_app_procedure.h"
#include "inc/cmd_response_callbacks.h"
#include "settings_core/sys_macros.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if (APP_MQTT_CMD_ARRAY_SIZE == 0)

void app_mqtt_commands_init(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_handler(void)
{

}

#else

#define ADD_CMD(cmd_id, _name, _param_type, _proc_id, _resp_send_cb)\
    [cmd_id] = {.name = _name, .param_type = _param_type, .procedure_id = _proc_id, \
                .send_resp_cb = _resp_send_cb }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct cmd_mqtt_init_s
{
    cmd_parameter_type_t    param_type;
    enum app_procedure      procedure_id;
    char                    *name;
    cmd_send_response_cb    send_resp_cb;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static cmd_procedure_t cmd_mqtt_array[APP_MQTT_CMD_ARRAY_SIZE] = { 0 };
static cmd_procedure_t *cmd_mqtt_ptr_array[APP_MQTT_CMD_ARRAY_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static cmd_procedure_ctrl_t cmd_ctrl_mqtt = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct cmd_mqtt_init_s cmd_init_array[APP_MQTT_CMD_ARRAY_SIZE] = 
{
    ADD_CMD(APP_MQTT_CMD_STATES, "states", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_STATES, app_mqtt_cmd_states_response),
    ADD_CMD(APP_MQTT_CMD_GET_PLC_IO, "get-plc-io", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_GET_PLC_IO, app_mqtt_cmd_get_plc_io_response),
    ADD_CMD(APP_MQTT_CMD_TYPE, "type", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_TYPE, app_mqtt_cmd_type_response),
    ADD_CMD(APP_MQTT_CMD_AC, "ac", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_AC, app_mqtt_cmd_ac_response),
    ADD_CMD(APP_MQTT_CMD_FAN, "fan", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_FAN, app_mqtt_cmd_fan_response),
    ADD_CMD(APP_MQTT_CMD_H_HEATER, "hangar_heater", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_H_HEAT, app_mqtt_cmd_hangar_heat_response),
    ADD_CMD(APP_MQTT_CMD_R_HEATER, "roof_heater", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_R_HEAT, app_mqtt_cmd_roof_heat_response),
    ADD_CMD(APP_MQTT_CMD_AIR_COMP, "air_compressor", CMD_PARAM_CHAR_ARRAY, 
            APP_PROCEDURE_AIR_COMP, app_mqtt_cmd_air_compressor_response)
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_initialize(cmd_procedure_t *cmd_arr, 
                           cmd_procedure_t **cmd_ptr_arr, 
                           struct cmd_mqtt_init_s *init_arr);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_init(void)
{
    static_assert(APP_MQTT_CMD_ARRAY_SIZE == APP_MQTT_CMD_NUM,
        "APP_MQTT_CMD_ARRAY_SIZE != APP_MQTT_CMD_NUM");
    
    cmd_initialize(cmd_mqtt_array, cmd_mqtt_ptr_array, cmd_init_array);
    
    cmd_procedure_ctrl_init(&cmd_ctrl_mqtt, cmd_mqtt_ptr_array, 
                            APP_MQTT_CMD_ARRAY_SIZE);
    log_info_m(log_controller, 
               "Application mqtt commands initalize succesfull!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_handler(void)
{
    cmd_procedure_ctrl_handler(&cmd_ctrl_mqtt);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool app_mqtt_cmd_call(enum app_mqtt_cmd cmd_id, char *mqtt_payload)
{    
    if(cmd_ctrl_mqtt.is_init == false)
    {
        return false;
    }
    if(cmd_id >= APP_MQTT_CMD_MIN && cmd_id <= APP_MQTT_CMD_MAX)
    {
        cmd_procedure_t *cmd_ptr = &cmd_mqtt_array[cmd_id];

        return cmd_procedure_call_char_arr(cmd_ptr, mqtt_payload);
    }    
    else
    {
        log_warning_m(log_controller, "Incorrect cmd_id [ %d ]!", cmd_id);
        return false;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_initialize(cmd_procedure_t *cmd_arr, 
                           cmd_procedure_t **cmd_ptr_arr, 
                           struct cmd_mqtt_init_s *init_arr)
{
    for(int cmd_id = 0; cmd_id < APP_MQTT_CMD_NUM; cmd_id++)
    {
        cmd_procedure_t *cmd_ptr = &cmd_arr[cmd_id];
        procedure_t *procedure_ptr = 
            app_procedure_get_procedure_ptr(init_arr[cmd_id].procedure_id);
        
        bool res = cmd_procedure_init(cmd_ptr, init_arr[cmd_id].param_type,
                                      procedure_ptr, init_arr[cmd_id].name);

        for(int act_id = 0; act_id < procedure_ptr->actions_num; act_id++)
        {
            struct procedure_action_s *act_ptr = procedure_ptr->actions[act_id];
            cmd_procedure_link_parameter_ch_arr_to_action_id(cmd_ptr, 
                act_ptr->name, act_id);
        }
        cmd_procedure_register_send_response_cb(cmd_ptr, 
                                                init_arr[cmd_id].send_resp_cb);

        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "MQTT cmd init err, cmd id [ %d ] not initialized!", cmd_id);    

        cmd_ptr_arr[cmd_id] = cmd_ptr;                                  
    }
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif