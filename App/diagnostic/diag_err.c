#include "inc/diag_err.h"
#include "debug/diagnostic/inc/diagnostic_ctrl.h"

#include "settings_app/general/settings_app_diag_codes.h"
#include "debug/log_modules/log_modules.h"
#include "peripherals_app/init/inc/app_peripheral_init.h"
#include "settings_core/sys_macros.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ENABLE_DIAG_ERR     0

#if (ENABLE_DIAG_ERR == 0)

void diagnostic_error_init(void)
{

}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_error_handler(void)
{

}

#else 

#define NO_STATE_HANDLER    NULL

#define DIAG_CSTATE (uint8_t)diag->current_state

#define PUB_MQTT_ERR(err_message) \
    hangar_mqtt_pub_msg_with_payload(HANGAR_PUB_MSG_DIAG_ERROR, err_message);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct diag_init_s
{
    char                        *name;
    diagnostic_check_state_cb   check_cb;
    diagnostic_change_state_cb  change_cb;
    diagnostic_state_handler_cb state_cb;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr);

static uint8_t diag_err_plc_state_check_state_cb(void);
static void diag_err_plc_state_change_state_cb(diagnostic_t *diag);

static uint8_t diag_err_mqtt_check_state_cb(void);
static void diag_err_mqtt_change_state_cb(diagnostic_t *diag);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct diag_init_s init_array[] = 
{
    {   .name = "err_plc", 
        .check_cb = diag_err_plc_state_check_state_cb,
        .change_cb = diag_err_plc_state_change_state_cb,
        .state_cb = NO_STATE_HANDLER
    },
    {   .name = "err_mqtt", 
        .check_cb = diag_err_mqtt_check_state_cb,
        .change_cb = diag_err_mqtt_change_state_cb,
        .state_cb = NO_STATE_HANDLER
    }
};

#define DIAG_ARRAY_SIZE (sizeof(init_array)/sizeof(init_array[0]))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static diagnostic_t diagnostic_array[DIAG_ARRAY_SIZE] = { 0 };
static diagnostic_t *diagnostic_ptr_array[DIAG_ARRAY_SIZE] = { 0 };

static diagnostic_ctrl_t err_diag_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_error_init(void)
{
    diag_initialize(diagnostic_array, diagnostic_ptr_array, init_array);
    notify_mqtt_err_init();

    diagnostic_ctrl_init(&err_diag_ctrl, diagnostic_ptr_array, 
                         DIAG_ARRAY_SIZE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_error_handler(void)
{
    diagnostic_ctrl_handler(&err_diag_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr)
{
    for(int diag_id = 0; diag_id < DIAG_ARRAY_SIZE; diag_id++)
    {
        diagnostic_t *diag_ptr = &diagnostic_array[diag_id];

        bool res = diagnostic_init(diag_ptr, init_arr[diag_id].name, 
                                   init_arr[diag_id].check_cb,
                                   init_arr[diag_id].change_cb,
                                   init_arr[diag_id].state_cb);
        
        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "App diag_err [ %d ] err initialized!", diag_id);
        
        diag_ptr_arr[diag_id] = diag_ptr;                     
    }   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t diag_err_plc_state_check_state_cb(void)
{
     return app_plc_get_state();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_err_plc_state_change_state_cb(diagnostic_t *diag)
{
    if(diag->current_state != PLC_STATE_OK)
    {
        PUB_MQTT_ERR("err_plc_communication");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t diag_err_mqtt_check_state_cb(void)
{
    return (uint8_t)hangar_mqtt_is_connected();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_err_mqtt_change_state_cb(diagnostic_t *diag)
{
    if(DIAG_CSTATE == 1) //mqtt connected
    {
        notify_mqtt_err_disable();
    }
    else    //mqtt not connected
    {
        notify_mqtt_err_enable();
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif