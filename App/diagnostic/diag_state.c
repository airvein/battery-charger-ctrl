#include "inc/diag_state.h"
#include "debug/diagnostic/inc/diagnostic_ctrl.h"
#include "settings_app/general/settings_app_diag_codes.h"
#include "debug/log_modules/log_modules.h"

#include "inc/app_diagnostic.h"
#include "peripherals_app/init/inc/app_peripheral_init.h"
#include "settings_core/sys_macros.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ENABLE_DIAG_STATE     0

#if (ENABLE_DIAG_STATE == 0)

void diagnostic_state_init(void)
{

}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_state_handler(void)
{

}

#else 

#define NO_STATE_HANDLER    NULL

#define DIAG_CSTATE (uint8_t)diag->current_state

#define ADD_DIAG(_name, _check_cb, _change_cb, _state_cb) \
    {\
        .name = _name, .check_cb = _check_cb, .change_cb = _change_cb,\
        .state_cb = _state_cb\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct diag_init_s
{
    char                        *name;
    diagnostic_check_state_cb   check_cb;
    diagnostic_change_state_cb  change_cb;
    diagnostic_state_handler_cb state_cb;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr);
static void plc_io_state_change_cb(enum hangar_pub_msg msg_id, uint8_t state);                            
static void peripheral_state_change_cb(enum hangar_pub_msg msg_id, 
                                       void (*fptr_get_state_str)(char*));

static void plc_state_change_cb(diagnostic_t *diag);
static void type_state_change_cb(diagnostic_t *diag);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct diag_init_s init_array[] = 
{
    ADD_DIAG("state-plc", app_plc_get_state, plc_state_change_cb, 
             NO_STATE_HANDLER)
};

#define DIAG_ARRAY_SIZE (sizeof(init_array)/sizeof(init_array[0]))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static diagnostic_t diagnostic_array[DIAG_ARRAY_SIZE] = { 0 };
static diagnostic_t *diagnostic_ptr_array[DIAG_ARRAY_SIZE] = { 0 };

static diagnostic_ctrl_t state_diag_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_state_init(void)
{
    diag_initialize(diagnostic_array, diagnostic_ptr_array, init_array);

    diagnostic_ctrl_init(&state_diag_ctrl, diagnostic_ptr_array, 
                         DIAG_ARRAY_SIZE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_state_handler(void)
{
    diagnostic_ctrl_handler(&state_diag_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr)
{
    for(int diag_id = 0; diag_id < DIAG_ARRAY_SIZE; diag_id++)
    {
        diagnostic_t *diag_ptr = &diagnostic_array[diag_id];

        bool res = diagnostic_init(diag_ptr, init_arr[diag_id].name, 
                                   init_arr[diag_id].check_cb,
                                   init_arr[diag_id].change_cb,
                                   init_arr[diag_id].state_cb);
        
        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "App diag_state [ %d ] err initialized!", diag_id);
        
        diag_ptr_arr[diag_id] = diag_ptr;                     
    }   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void plc_state_change_cb(diagnostic_t *diag)
{
    UNUSED(diag);
    peripheral_state_change_cb(HANGAR_PUB_MSG_PLC_STATE, 
                               app_plc_get_state_str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void type_state_change_cb(diagnostic_t *diag)
{
    if(diag->current_state <= CTRL_AUTO)
    {
        char *state_str[] = 
        {
            [CTRL_MANUAL] = "manual", [CTRL_AUTO] = "auto"
        };
        hangar_mqtt_pub_msg_with_payload(HANGAR_PUB_MSG_STATE_TYPE,
                                         state_str[diag->current_state]);
    }
    else
    {
        log_warning_m(log_controller, 
            "Diag states: Incorrect type control value [ %d ]", 
            diag->current_state);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void peripheral_state_change_cb(enum hangar_pub_msg msg_id, 
                                       void (*fptr_get_state_str)(char*))
{
    char state_str[50] = { 0 };
    fptr_get_state_str(state_str);
    hangar_mqtt_pub_msg_with_payload(msg_id, state_str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t  ac_state_check_cb(void)
{
    return app_plc_get_output_state(APP_PLC_OUT_AC);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void     ac_state_change_cb(diagnostic_t *diag)
{
    plc_io_state_change_cb(HANGAR_PUB_MSG_STATE_AC, diag->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t  fan_state_check_cb(void)
{
    return app_plc_get_output_state(APP_PLC_OUT_FAN);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void     fan_state_change_cb(diagnostic_t *diag)
{
    plc_io_state_change_cb(HANGAR_PUB_MSG_STATE_FAN, diag->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t  hangar_heat_state_check_cb(void)
{
    return app_plc_get_output_state(APP_PLC_OUT_HANGAR_HEATER);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void     hangar_heat_state_change_cb(diagnostic_t *diag)
{
    plc_io_state_change_cb(HANGAR_PUB_MSG_STATE_H_HEAT, diag->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t  roof_heat_state_check_cb(void)
{
    return app_plc_get_output_state(APP_PLC_OUT_ROOF_HEATER);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void     roof_heat_state_change_cb(diagnostic_t *diag)
{
    plc_io_state_change_cb(HANGAR_PUB_MSG_STATE_R_HEAT, diag->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t  air_comp_state_check_cb(void)
{
    return app_plc_get_output_state(APP_PLC_OUT_AIR_COMP);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void     air_comp_state_change_cb(diagnostic_t *diag)
{
    plc_io_state_change_cb(HANGAR_PUB_MSG_STATE_AIR_COMP, diag->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void plc_io_state_change_cb(enum hangar_pub_msg msg_id, uint8_t state)
{       
    char *state_str = state == ON ? "enable" : "disable";
    hangar_mqtt_pub_msg_with_payload(msg_id, state_str);
} 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif