#include "inc/diag_warn.h"
#include "debug/diagnostic/inc/diagnostic_ctrl.h"
#include "settings_app/general/settings_app_diag_codes.h"
#include "debug/log_modules/log_modules.h"

#include "inc/app_diagnostic.h"
#include "peripherals_app/init/inc/app_peripheral_init.h"
#include "settings_core/sys_macros.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ENABLE_DIAG_WARN     0

#if (ENABLE_DIAG_WARN == 0)

void diagnostic_warning_init(void)
{

}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_warning_handler(void)
{

}

#else 

#define NO_STATE_HANDLER    NULL

#define DIAG_CSTATE (uint8_t)diag->current_state

#define PUB_MQTT_WARN(warn_msg)\
    hangar_mqtt_pub_msg_with_payload(HANGAR_PUB_MSG_DIAG_WARNING, warn_msg);

#define ADD_DIAG(_name, _check_cb, _change_cb, _state_cb) \
    {\
        .name = _name, .check_cb = _check_cb, .change_cb = _change_cb,\
        .state_cb = _state_cb\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct diag_init_s
{
    char                        *name;
    diagnostic_check_state_cb   check_cb;
    diagnostic_change_state_cb  change_cb;
    diagnostic_state_handler_cb state_cb;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr);

static void clim_A_sensor_disconnected_change_cb(diagnostic_t *diag);
static void clim_B_sensor_disconnected_change_cb(diagnostic_t *diag);
static void sensor_bm280_disconnected_change_cb(uint8_t state, 
                                                char sensor_sign);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct diag_init_s init_array[] = 
{
    ADD_DIAG("warn-sensA", app_peripheral_sensor_A_get_state, 
             clim_A_sensor_disconnected_change_cb, NO_STATE_HANDLER),
    ADD_DIAG("warn-sensB", app_peripheral_sensor_B_get_state, 
             clim_B_sensor_disconnected_change_cb, NO_STATE_HANDLER)
};

#define DIAG_ARRAY_SIZE (sizeof(init_array)/sizeof(init_array[0]))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static diagnostic_t diagnostic_array[DIAG_ARRAY_SIZE] = { 0 };
static diagnostic_t *diagnostic_ptr_array[DIAG_ARRAY_SIZE] = { 0 };

static diagnostic_ctrl_t warn_diag_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_warning_init(void)
{
    diag_initialize(diagnostic_array, diagnostic_ptr_array, init_array);

    diagnostic_ctrl_init(&warn_diag_ctrl, diagnostic_ptr_array, 
                         DIAG_ARRAY_SIZE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_warning_handler(void)
{
    diagnostic_ctrl_handler(&warn_diag_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void diag_initialize(diagnostic_t *diag_arr, 
                            diagnostic_t **diag_ptr_arr,
                            struct diag_init_s *init_arr)
{
    for(int diag_id = 0; diag_id < DIAG_ARRAY_SIZE; diag_id++)
    {
        diagnostic_t *diag_ptr = &diagnostic_array[diag_id];

        bool res = diagnostic_init(diag_ptr, init_arr[diag_id].name, 
                                   init_arr[diag_id].check_cb,
                                   init_arr[diag_id].change_cb,
                                   init_arr[diag_id].state_cb);
        
        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "App diag_warn [ %d ] err initialized!", diag_id);
        
        diag_ptr_arr[diag_id] = diag_ptr;                     
    }   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void clim_A_sensor_disconnected_change_cb(diagnostic_t *diag)
{
    sensor_bm280_disconnected_change_cb(diag->current_state, 'A');
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void clim_B_sensor_disconnected_change_cb(diagnostic_t *diag)
{
    sensor_bm280_disconnected_change_cb(diag->current_state, 'B');
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void sensor_bm280_disconnected_change_cb(uint8_t state, 
                                                char sensor_sign)
{
    if(state == BME280_DISCONNECTED)
    {
        char warn_payload[50] = { 0 };
        sprintf(warn_payload, "warn_bme_sensor_%c_disconnected", sensor_sign);
        PUB_MQTT_WARN(warn_payload);
    }
}                           

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif