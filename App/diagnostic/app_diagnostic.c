#include "inc/app_diagnostic.h"
#include "debug/log_modules/log_modules.h"

#include "inc/diag_state.h"
#include "inc/diag_warn.h"
#include "inc/diag_err.h"

#include <stdlib.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_diagnostics_init(void)
{ 
    diagnostic_state_init();
    diagnostic_warning_init();
    diagnostic_error_init();

    log_info_m(log_controller, 
                "Application diagnostics initalize succesfull!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_diagnostics_handler(void)
{
    diagnostic_error_handler();
    diagnostic_warning_handler();
    diagnostic_state_handler();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|