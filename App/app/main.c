/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main battery_charger_ctrl program file
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 Dronehub
  */

#include "inc/main.h"

#include "peripherals_core/rcc/inc/rcc.h"
#include "peripherals_core/heartbeat_led/inc/heartbeat_led.h"

#include "communication/init/inc/hardware_communication.h"
#include "communication/hardware/can/inc/can_open.h"
#include "communication_interface/canbus/inc/canbus.h"

#include "debug/init/inc/hardware_debug.h"
#include "debug/log_modules/log_modules.h"
#include "debug/interface/inc/uart_printf.h"
#include "debug/diagnostic/inc/diagnostic.h"
#include "debug/diagnostic/inc/notify_signal.h"

#include "time/timeout/inc/timeout.h"
#include "time/time_event/inc/time_event.h"

#include "commands/inc/app_mqtt_commands.h"
#include "procedures/inc/app_procedure_ctrl.h"
#include "app_time/inc/app_timeouts.h"
#include "app_time/inc/app_time_events.h"
#include "peripherals_app/init/inc/app_peripheral_init.h"
#include "diagnostic/inc/app_diagnostic.h"

#include "settings_core/global_settings_gpio.h"
#include "settings_core/global_settings_timer.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	notify_timer_period_elapsed(htim);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void start_system_log(void)
{
	log_info_m(log_controller, " ");
	log_info_m(log_controller, "---------------------------------------");
	log_info_m(log_controller, "------------ SYSTEM START -------------");
	log_info_m(log_controller, "---------------------------------------");
	log_info_m(log_controller, " ");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int main(void)
{
	HAL_Init();
    SystemClock_Config();
	
	__SYSTICK_DISABLE;
	SysTick_Config(SYSTICK_IRQ_TICK);
	__SYSTICK_DISABLE;

    init_log_modules();
	debug_hardware_init();

    log_register_printf_clbk(uart_printf);
    log_set_global_log_level(LOG_GLOBAL_SETTINGS);
	start_system_log();

    communication_hardware_init();
	can_hardware_init();

	heartbeat_led_init();   	
	app_peripheral_init();
	app_procedure_ctrl_init();
	app_mqtt_commands_init();
	app_timeout_init();
	app_time_events_init();
	app_diagnostics_init();
	canbus_init();
	__SYSTICK_ENABLE;

	while (1)
	{
		app_mqtt_commands_handler();
		app_procedure_ctrl_handler();
		app_time_events_exec_handler();
		app_diagnostics_handler();
		canbus_msg_execute_handler();
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void Error_Handler(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
