#ifndef __APP_PROCEDURE_CTRL_H
#define __APP_PROCEDURE_CTRL_H

#include "control/procedure/inc/procedure_ctrl.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum app_procedure
{
    __APP_PROCEDURE_MIN = 0,

    /* procedures enums start */
    APP_PROCEDURE_STATES = __APP_PROCEDURE_MIN,

    /* procedures enums end */
    __APP_PROCEDURE_LAST,
    __APP_PROCEDURE_MAX =  __APP_PROCEDURE_LAST - 1,
    __APP_PROCEDURE_NUM
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/* update array size */
#define APP_PROCEDURE_ARRAY_SIZE    0

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef bool(*fptr_procedure_init)(procedure_t *procedure_ptr, uint32_t id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_init(void);

void app_procedure_ctrl_handler(void);

procedure_t *app_procedure_get_procedure_ptr(enum app_procedure proc_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_PROCEDURE_CTRL_H */