#include "inc/app_procedure_ctrl.h"
#include "control/procedure/inc/procedure_ctrl.h"
#include "obj/inc/procedures_obj_list.h"

#include "debug/log_modules/log_modules.h"
#include "settings_core/sys_macros.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if (APP_PROCEDURE_ARRAY_SIZE == 0)

void app_procedure_ctrl_init(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_handler(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t *app_procedure_get_procedure_ptr(enum app_procedure proc_id)
{
    return NULL;
}

#else

#define ADD_PROCEDURE(proc_id, fptr_init) \
    [proc_id] = fptr_init

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_t procedure_array[APP_PROCEDURE_ARRAY_SIZE] = { 0 };
static procedure_t *procedure_ptr_array[APP_PROCEDURE_ARRAY_SIZE] = { 0 };

static procedure_ctrl_t precedure_ctrl = { 0 };

static fptr_procedure_init init_array[APP_PROCEDURE_ARRAY_SIZE] =
{
    ADD_PROCEDURE(APP_PROCEDURE_STATES, states_procedure_init),
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void procedures_initialize(procedure_t *proc_arr, 
                                  procedure_t **proc_ptr_arr,
                                  fptr_procedure_init *init_arr);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_init(void)
{
    static_assert(APP_PROCEDURE_ARRAY_SIZE == __APP_PROCEDURE_NUM,
        "APP_PROCEDURE_ARRAY_SIZE != __APP_PROCEDURE_NUM");

    procedures_initialize(procedure_array, procedure_ptr_array, init_array);

    procedure_ctrl_init(&precedure_ctrl, procedure_ptr_array, 
                        APP_PROCEDURE_ARRAY_SIZE);
    log_info_m(log_controller, 
                "Application procedure initialize succesfull!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_handler(void)
{
    procedure_ctrl_handler(&precedure_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t *app_procedure_get_procedure_ptr(enum app_procedure proc_id)
{
    if(proc_id >= __APP_PROCEDURE_MIN && proc_id <= __APP_PROCEDURE_MAX)
    {
        return &procedure_array[proc_id];
    }
    else
    {
        return NULL;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void procedures_initialize(procedure_t *proc_arr, 
                                  procedure_t **proc_ptr_arr,
                                  fptr_procedure_init *init_arr)
{
    for(uint32_t proc_id = 0; proc_id < __APP_PROCEDURE_NUM; proc_id++)
    {
        procedure_t *procedure_ptr = &proc_arr[proc_id];

        bool res = init_arr[proc_id](procedure_ptr, proc_id);

        CHECK_AND_INF_LOOP_MSG(res == false, log_error_g, 
            "App procedure init err, procedure id [ %d ] not initialized!", 
            proc_id);
        
        proc_ptr_arr[proc_id] = procedure_ptr;
    }
}                           

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif