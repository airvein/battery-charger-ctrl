#ifndef __SETTINGS_LOG_H
#define __SETTINGS_LOG_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_CTRL_NAME					"battery-charger-ctrl"

#define _LOG_SETTINGS_(D,I,W,E)			(LOG_ENA_DBG(D) | LOG_ENA_INFO(I) \
							            | LOG_ENA_WARN(W) | LOG_ENA_ERR(E))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

									/*  _LOG_SETTINGS_(D,I,W,E)  */
#define LOG_GLOBAL_SETTINGS             _LOG_SETTINGS_(1,1,1,1)
													  
#define LOG_PROCEDURE_SETTINGS          _LOG_SETTINGS_(0,1,1,1)
#define LOG_PROC_CTRL_SETTINGS          _LOG_SETTINGS_(0,1,1,1)
#define LOG_CMD_PROC_SETTINGS           _LOG_SETTINGS_(0,1,1,1)
#define LOG_CMD_PROC_CTRL_SETTINGS      _LOG_SETTINGS_(0,1,1,1)
#define LOG_DIAGNOSTIC_SETTINGS			_LOG_SETTINGS_(0,1,1,1)
#define LOG_DIAGNOSTIC_CTRL_SETTINGS	_LOG_SETTINGS_(0,1,1,1)
#define LOG_NOTIFY_SETTINGS				_LOG_SETTINGS_(0,1,1,1)
#define LOG_TIMEOUT_SETTINGS			_LOG_SETTINGS_(0,1,1,1)
#define LOG_TIME_EVENT_SETTINGS			_LOG_SETTINGS_(0,1,1,1)
#define LOG_CAN_MSG_SETTINGS			_LOG_SETTINGS_(0,1,1,1)
#define LOG_CONTROLLER_SETTINGS			_LOG_SETTINGS_(0,1,1,1)
							            
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_LOG_H */
