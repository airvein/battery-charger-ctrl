#ifndef __HANGAR_HEARTBEAT_H
#define __HANGAR_HEARTBEAT_H

#include "settings_app/communication/settings_mqtt.h"
#include "communication/protocols/mqtt/inc/mqtt_msg.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define DISABLE     0
#define ENABLE      1

#define HANGAR_HEARTBEAT_ENABLE ENABLE

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if HANGAR_HEARTBEAT_ENABLE
#define HANGAR_HEARTBEAT_MQTT_TOPIC_SUB     \
                                        MQTT_MODULE_NAME"/cmd/heartbeat/call"

#define HANGAR_HEARTBEAT_MQTT_TOPIC_PUB     \
                                        MQTT_MODULE_NAME"/cmd/heartbeat/resp"

#define HANGAR_HEARTBEAT_RESPONSE_PAYLOAD   "tock"
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __HANGAR_HEARTBEAT_H */