#ifndef __SETTINGS_APP_TIMEOUTS_H
#define __SETTINGS_APP_TIMEOUTS_H

#include "settings_core/global_settings_timer.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define __APP_TOUT_SEC(sec)     SYSTICK_INTERVAL_SEC(sec)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_TOUT_CTRL_RESET_TIM_SEC             __APP_TOUT_SEC(5)
#define APP_TOUT_AC_CTRL_TIM_SEC                __APP_TOUT_SEC(10)
#define APP_TOUT_H_HEATER_CTRL_TIM_SEC          __APP_TOUT_SEC(10)
#define APP_TOUT_R_HEATER_CTRL_TIM_SEC          __APP_TOUT_SEC(10)
#define APP_TOUT_FAN_CTRL_TIM_SEC               __APP_TOUT_SEC(10)
#define APP_TOUT_AIR_COMP_CTRL_TIM_SEC          __APP_TOUT_SEC(10)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_APP_TIMEOUTS_H */