#ifndef __SETTINGS_APP_DIAG_CODES_H
#define __SETTINGS_APP_DIAG_CODES_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
                            /* warnings code */

                            /* telemetry */


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
                            /* errors code */

                            /* peripheral */
#define DIAG_CODE_ERR_PLC_STATE                     "CGEPE1"
#define DIAG_CODE_ERR_WEIGHT_CTRL_COM               "CGEPE2"
                            /* cmd_control */


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_APP_DIAG_CODES_H */