#ifndef __SETTINGS_APP_PROCEDURE_H
#define __SETTINGS_APP_PROCEDURE_H

#include "control/procedure/inc/procedure.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define FORCE                                       (uint8_t)'f'
#define CARGO                                       (uint8_t)'c'
#define SERVICE                                     (uint8_t)'s'

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_PROC_TYPE_NAME                          "type"
#define APP_PROC_TYPE_ACT_AUTO                      "auto"
#define APP_PROC_TYPE_ACT_MANUAL                    "manual"

#define APP_PROC_ACT_ENABLE                         "enable"
#define APP_PROC_ACT_DISABLE                        "disable"

#define APP_PROC_STATES_NAME                        "states"
#define APP_PROC_STATES_ACT_GET                     "get"

#define APP_PROC_PLC_IO_NAME                        "plc_io"
#define APP_PROC_PLC_IO_ACT_GET                     "get"

#define APP_PROC_AC_NAME                            "ac"
#define APP_PROC_H_HEATER_NAME                      "hangar_heater"
#define APP_PROC_R_HEATER_NAME                      "roof_heater"
#define APP_PROC_FAN_NAME                           "fan"
#define APP_PROC_AIR_COMP_NAME                      "air_comp"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct action_init_s
{
    char *name;
    struct action_callbacks_s act_cb;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ADD_ACTION(_name, _feas_cb, _exec_cb, _eval_cb, _done_cb, _err_cb) \
    {\
        .name = _name, \
        ACTION_CALLBACKS(_feas_cb, _exec_cb, _eval_cb, _done_cb, _err_cb)\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif /* __SETTINGS_APP_PROCEDURE_H */