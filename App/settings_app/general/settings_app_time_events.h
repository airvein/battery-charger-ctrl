#ifndef __SETTINGS_APP_TIME_EVENTS_H
#define __SETTINGS_APP_TIME_EVENTS_H

#include "settings_core/global_settings_timer.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define __APP_TEVT_SEC(sec)     SYSTICK_INTERVAL_SEC(sec)

#define APP_TEVT_TEST_SEC       __APP_TEVT_SEC(1)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_APP_TIME_EVENTS_H */