#include "inc/can_msg.h"
#include "debug/log_modules/log_modules.h"
#include <string.h>
#include <stdio.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC()\
    { log_debug_m(log_can_msg, "%s : %s", __FILE__, __func__); }

#define CAN_MSG_EXT_ID_MAX      0x1FFFFFFF
#define CAN_MSG_STD_ID_MAX      0x7FF

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(can_msg_t *msg, 
    CAN_HandleTypeDef *hcan, uint32_t id, can_msg_handler_cb handler_cb,
    can_msg_remote_resp_cb remote_cb);
static bool is_initailize_msg(can_msg_t *msg);
static bool is_correct_data_to_send(uint8_t *data, uint8_t data_len);
static void prepare_tx_header_for_send_msg(can_msg_t *msg, uint32_t rtr,
                                           CAN_TxHeaderTypeDef *tx_header,
                                           uint8_t data_len);
static bool set_recv_data(can_msg_data_t *msg_recv_data, uint8_t *data, 
                            uint8_t data_len);         
static bool send_msg_remote_response(can_msg_t *msg);

static void print_send_data(uint32_t id, uint8_t *data, uint8_t data_len);
static void print_send_remote(uint32_t id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_init(can_msg_t *msg, CAN_HandleTypeDef *hcan, uint32_t id, 
                  can_msg_handler_cb handler_cb, 
                  can_msg_remote_resp_cb remote_cb)
{
    LOG_DEBUG_FUNC();
    if(is_correct_initialize_parameters(msg, hcan, id, handler_cb, remote_cb) 
        == false)
    {
        return false;
    }

    msg->hcan = hcan;    
    msg->id = id;

    if(id > CAN_MSG_STD_ID_MAX)
    {
        msg->ide = CAN_ID_EXT;
    }
    else
    {
        msg->ide = CAN_ID_STD;
    }
    
    memset(msg->recv_data.data, 0, CAN_DATA_MAX_LEN);
    msg->recv_data.data_len = 0;

    msg->is_order = false;
    msg->order_id = 0;

    msg->handler_cb = handler_cb;
    msg->remote_resp_cb = remote_cb;

    msg->is_init = true;    
    log_info_m(log_can_msg, "Correct initialized can_msg with id [ 0x%08X ]",
                msg->id);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_send_data(can_msg_t *msg, uint8_t *data, uint8_t data_len)
{
    LOG_DEBUG_FUNC();
    if(is_initailize_msg(msg) == false)
    {
        log_warning_m(log_can_msg, "Try send NULL ptr can msg!");
        return false;
    }
    
    if(is_correct_data_to_send(data, data_len) == false)
    {
        log_warning_m(log_can_msg, 
            "Incorrect send data parameter for can_msg id [ 0x%08X ]",
            msg->id);
        return false;
    }

    uint32_t tx_mailbox;
    CAN_TxHeaderTypeDef tx_header = { 0 };
    prepare_tx_header_for_send_msg(msg, CAN_RTR_DATA, &tx_header, 
                                   data_len);
    print_send_data(msg->id, data, data_len);
    HAL_CAN_AddTxMessage(msg->hcan, &tx_header, data, &tx_mailbox);
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_send_remote_req(can_msg_t *msg)
{
    LOG_DEBUG_FUNC();
    if(is_initailize_msg(msg) == false)
    {
        log_warning_m(log_can_msg, "Try send NULL ptr can msg!");
        return false;
    }

    uint32_t tx_mailbox;
    CAN_TxHeaderTypeDef tx_header = { 0 };
    prepare_tx_header_for_send_msg(msg, CAN_RTR_REMOTE, &tx_header, 0);    
    print_send_remote(msg->id);
    HAL_CAN_AddTxMessage(msg->hcan, &tx_header, NULL, &tx_mailbox);
    
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_execute(can_msg_t *msg)
{
    if(is_initailize_msg(msg) == false)
    {
        return false;
    }
    if(msg->is_order == true)
    {
        if(msg->handler_cb(msg) == CAN_MSG_HANDLE_DONE)
        {
            msg->is_order = false;
        }
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_order(can_msg_t *msg, uint32_t rtr, uint8_t *data, 
                   uint8_t data_len)
{
    LOG_DEBUG_FUNC();
    if(is_initailize_msg(msg) == false)
    {
        return false;
    }

    if(rtr == CAN_RTR_REMOTE)
    {
        log_info_m(log_can_msg, "Order remote req for msg_id [ 0x%08X ]", 
                    msg->id);
        send_msg_remote_response(msg);

        return true;
    }

    if(msg->is_order == false)
    {
        msg->is_order = true;
        msg->order_id++;
        set_recv_data(&msg->recv_data, data, data_len);

        // log_info_m(log_can_msg, "can_msg with [ 0x%08X ] order!", msg->id);
        return true;
    }
    else
    {
        log_warning_m(log_can_msg, "can_msg [ 0x%08X ] already ordered!", msg->id);
        return false;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(can_msg_t *msg, 
    CAN_HandleTypeDef *hcan, uint32_t id, can_msg_handler_cb handler_cb,
    can_msg_remote_resp_cb remote_cb)
{
    LOG_DEBUG_FUNC();
    if(msg == NULL)
    {
        log_warning_m(log_can_msg, "Try initialize NULL ptr can msg!");
        return false;
    }
    if(hcan == NULL)
    {
        log_warning_m(log_can_msg, "HandleCan pointer is NULL!");
        return false;
    }
    if(id > CAN_MSG_EXT_ID_MAX)
    {
        log_warning_m(log_can_msg, 
                    "CAN msg id init value [ 0x%08X ] is greater than max "
                    "EXT_ID standard value [ 0x%08X ]",
                    id, CAN_MSG_EXT_ID_MAX);
        return false;
    }
    if(handler_cb == NULL)
    {
        log_warning_m(log_can_msg,
                    "CAN msg handler callback is NULL!");
        return false;
    }
    if(remote_cb == NULL)
    {
        log_warning_m(log_can_msg,
                    "CAN msg remote response callback is NULL!");
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_initailize_msg(can_msg_t *msg)
{
    return msg != NULL && msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_data_to_send(uint8_t *data, uint8_t data_len)
{
    LOG_DEBUG_FUNC();
    return data != NULL && (data_len > 0 && data_len <= 8);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void prepare_tx_header_for_send_msg(can_msg_t *msg, uint32_t rtr,
                                           CAN_TxHeaderTypeDef *tx_header,
                                           uint8_t data_len)
{
    LOG_DEBUG_FUNC();

    tx_header->RTR = rtr;    
    tx_header->IDE = msg->ide;

    if(msg->ide == CAN_ID_STD)
    {
        tx_header->StdId = msg->id;
    }
    else
    {
        tx_header->ExtId = msg->id;
    }    
    tx_header->DLC = data_len;
}                                           

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool set_recv_data(can_msg_data_t *msg_recv_data, uint8_t *data, 
                            uint8_t data_len)
{
    LOG_DEBUG_FUNC();
    memcpy(msg_recv_data->data, data, data_len);
    msg_recv_data->data_len = data_len;

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool send_msg_remote_response(can_msg_t *msg)
{
    LOG_DEBUG_FUNC();

    can_msg_remote_resp_data_t remote_resp = { 0 };
    remote_resp = msg->remote_resp_cb();   

    char data_str[35] = { 0 };
    for(int i = 0; i < remote_resp.data_len; i++)
    {
        if(i == remote_resp.data_len-1)
            sprintf(data_str+strlen(data_str), "%d", remote_resp.data[i]);
        else
            sprintf(data_str+strlen(data_str), "%d,", remote_resp.data[i]);
    }
    log_debug_m(log_can_msg, "Sending remote response for msg_id [ 0x%08X ] "
        "{ data_len/data: [ %d ]/[ %s ]", 
        msg->id, remote_resp.data_len, data_str);

    return can_msg_send_data(msg, remote_resp.data, remote_resp.data_len);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void print_send_data(uint32_t id, uint8_t *data, uint8_t data_len)
{
    char data_str[35] = "";
    if(data_len == 0)
    {
        sprintf(data_str+strlen(data_str), "empty_data");
    }
    else
    {
        for(int i = 0; i < data_len; i++)
        {
            if(i == data_len-1)
            {
                sprintf(data_str + strlen(data_str), "%d", 
                        data[i]);
            }
            else
            {
                sprintf(data_str + strlen(data_str), "%d, ", 
                        data[i]);
            }
        }
    }
    log_debug_m(log_can_msg, 
    "Send can_msg [DATA] { id: [ 0x%08X ], data_len: [ %d ], data: [ %s ] }",  
    id, data_len, data_str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void print_send_remote(uint32_t id)
{
    log_debug_m(log_can_msg, 
        "Send can_msg [REMOTE] { id: [ 0x%08X ]}",  id);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|