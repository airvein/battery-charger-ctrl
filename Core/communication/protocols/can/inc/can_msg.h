#ifndef __CAN_MSG_H
#define __CAN_MSG_H

#ifdef STM32F767xx
    #include "stm32f1xx_hal.h"
#elif STM32F446xx
    #include "stm32f4xx_hal.h"
#elif STM32F103xB
    #include "stm32f1xx_hal.h"
    #include "stm32f1xx_hal_can.h"
#endif

#include <stdbool.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CAN_DATA_MAX_LEN    8

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    CAN_MSG_HANDLE_PENDING,
    CAN_MSG_HANDLE_DONE
    
} can_msg_handle_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _can_msg_s can_msg_t;

typedef struct _can_msg_remote_resp_data_s can_msg_remote_resp_data_t;


typedef can_msg_handle_t (*can_msg_handler_cb)(can_msg_t*);

typedef can_msg_remote_resp_data_t (*can_msg_remote_resp_cb)(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _can_msg_remote_resp_data_s
{
    uint8_t *data;
    uint8_t data_len;
    
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct
{
    uint32_t data_len;
    uint8_t data[CAN_DATA_MAX_LEN];

} can_msg_data_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _can_msg_s
{
    char name[25];

    CAN_HandleTypeDef *hcan;
    uint32_t id;
    uint32_t ide;
    
    can_msg_data_t recv_data;
    
    bool is_order;
    uint32_t order_id;

    can_msg_handler_cb handler_cb;
    can_msg_remote_resp_cb remote_resp_cb;

    bool is_init;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_init(can_msg_t *msg, CAN_HandleTypeDef *hcan, uint32_t id, 
                  can_msg_handler_cb handler_cb, 
                  can_msg_remote_resp_cb remote_cb);

bool can_msg_send_data(can_msg_t *msg, uint8_t *data, uint8_t data_len);

bool can_msg_send_remote_req(can_msg_t *msg);

bool can_msg_execute(can_msg_t *msg);

bool can_msg_order(can_msg_t *msg, uint32_t rtr, uint8_t *data, 
                   uint8_t data_len);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CAN_MSG_H */
