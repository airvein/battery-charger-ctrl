#ifndef __CAN_MSG_CTRL_H
#define __CAN_MSG_CTRL_H

#include "can_msg.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _can_msg_ctrl_s
{
    can_msg_t **msg_array;
    uint16_t msg_array_size;

    bool is_init;    

} can_msg_ctrl_t;


typedef struct _can_msg_recv_s
{
    uint32_t order_msg_id;
    uint32_t rtr;
    uint8_t *data;
    uint8_t data_len;

} can_msg_recv_t;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_ctrl_init(can_msg_ctrl_t *ctrl, can_msg_t **msg_array, 
                       uint16_t array_size);

bool can_msg_ctrl_order_handler(can_msg_ctrl_t *ctrl, can_msg_recv_t recv_msg);

bool can_msg_ctrl_execute_handler(can_msg_ctrl_t *ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CAN_MSG_CTRL_H */