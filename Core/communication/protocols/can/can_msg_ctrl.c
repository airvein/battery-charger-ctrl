#include "inc/can_msg_ctrl.h"
#include "debug/log_modules/log_modules.h"
#include <stdio.h>
#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(can_msg_t **msg_array, 
                                             uint16_t array_size);
static bool is_ctrl_initialized(can_msg_ctrl_t *ctrl);
static void print_recv_can_msg(can_msg_recv_t recv_msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_ctrl_init(can_msg_ctrl_t *ctrl, can_msg_t **msg_array, 
                       uint16_t array_size)
{
    if(ctrl == NULL)
    {
        log_error_m(log_can_msg_ctrl, 
            "Can't init can_msg_ctrl [ ctrl ptr is NULL]");
        return false;
    }
    if(is_correct_initialize_parameters(msg_array, array_size) == false)
    {
        return false;
    }
    
    ctrl->msg_array = msg_array;
    ctrl->msg_array_size = array_size;
    ctrl->is_init = true;
    log_info_m(log_can_msg_ctrl, "Correct initialize can_msg_ctrl!");

    return true;
}                       

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_ctrl_order_handler(can_msg_ctrl_t *ctrl, can_msg_recv_t recv_msg)
{
    if(is_ctrl_initialized(ctrl) == false)
    {
        return false;
    }

    // print_recv_can_msg(recv_msg);

    for(int msg_id = 0; msg_id < ctrl->msg_array_size; msg_id++)
    {
        can_msg_t *msg = ctrl->msg_array[msg_id];

        if(msg->id == recv_msg.order_msg_id)
        {
            return can_msg_order(msg, recv_msg.rtr, recv_msg.data, 
                                recv_msg.data_len);
        }
    }
    log_warning_m(log_can_msg_ctrl, 
        "Receive can_msg [ 0x%08X ] is not lacated in can_msg_ctrl messages!",
        recv_msg.order_msg_id);
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool can_msg_ctrl_execute_handler(can_msg_ctrl_t *ctrl)
{
    if(is_ctrl_initialized(ctrl) == false)
    {
        return false;
    }

    for(int msg_id = 0; msg_id < ctrl->msg_array_size; msg_id++)
    {
        can_msg_t *msg = ctrl->msg_array[msg_id];
        can_msg_execute(msg);
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(can_msg_t **msg_array, 
                                             uint16_t array_size)
{
    if(msg_array == NULL)
    {
        log_error_m(log_can_msg_ctrl, "Init parameter [ msg_array ] is NULL!");
        return false;
    }
    if(array_size == 0)
    {
        log_error_m(log_can_msg_ctrl, 
            "Init parameter [ array_size ] is equal 0");
        return false;
    }
    for(int msg_id = 0; msg_id < array_size; msg_id++)
    {
        can_msg_t *msg = msg_array[msg_id];
        if(msg == NULL || msg->is_init == false)
        {
            log_error_m(log_can_msg_ctrl, 
                "Init parameter [ can_msg 0x%08X ] in msg_array "
                "is not initalized!", msg_id);
            return false;
        }
    }
    return true;
}                                             

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_ctrl_initialized(can_msg_ctrl_t *ctrl)
{
    return ctrl != NULL && ctrl->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
__attribute__((unused))
static void print_recv_can_msg(can_msg_recv_t recv_msg)
{
    if(recv_msg.rtr == CAN_RTR_REMOTE)
    {
        log_info_m(log_can_msg_ctrl, 
            "Receive can_msg [ REMOTE ] { id: [ 0x%08X ] }",  
            recv_msg.order_msg_id);
    }
    else
    {
        char data_str[35] = "";

        if(recv_msg.data_len == 0)
        {
            sprintf(data_str+strlen(data_str), "empty_data");
        }
        else
        {
            for(int i = 0; i < recv_msg.data_len; i++)
            {
                if(i == recv_msg.data_len-1)
                {
                    sprintf(data_str + strlen(data_str), "%d", 
                            recv_msg.data[i]);
                }
                else
                {
                    sprintf(data_str + strlen(data_str), "%d, ", 
                            recv_msg.data[i]);
                }
            }
        }
        
        log_info_m(log_can_msg_ctrl, 
            "Receive can_msg [ DATA ] { id: [ 0x%08X ], "
            "data_len: [ %d ], data: [ %s ] }",  
            recv_msg.order_msg_id, recv_msg.data_len, data_str);
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|