#include "settings_app/communication/settings_can.h"
#include "settings_core/communication/settings_can.h"
#include "communication/hardware/can/inc/can_open.h"

#include "debug/log/log.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

CAN_HandleTypeDef hcan1;
uint32_t TxMailbox;
uint32_t a, r;
CAN_FilterTypeDef sFilterConfig;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_gpio_init();
void can_peripherials_init();


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_hardware_init()
{
	can_gpio_init();
	can_peripherials_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_filters_init(uint8_t ctrl_id, uint16_t msg_id_offset)
{
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;

	uint16_t filter_id = ctrl_id * msg_id_offset;	
	const uint16_t filter_mask = 0x7F0;
	
	sFilterConfig.FilterIdHigh = (filter_id << 5);
	sFilterConfig.FilterIdLow = 0;

	sFilterConfig.FilterMaskIdHigh = (filter_mask << 5);
	sFilterConfig.FilterMaskIdLow = 0;

	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterActivation = ENABLE;

	ASSERT_LOG_ERROR_G(HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK,
		"HAL_CAN_ConfigFilter ERR");

	ASSERT_LOG_ERROR_G(HAL_CAN_Start(&hcan1) != HAL_OK, 
		"HAL_CAN_Start ERR");
	
	ASSERT_LOG_ERROR_G(
		HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) 
			!= HAL_OK, 
		"HAL_CAN_ActivateNotification ERR");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_gpio_init()
{
	__HAL_RCC_CAN1_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**CAN GPIO Configuration
    PB8     ------> CAN_RX
    PB9     ------> CAN_TX
    */
   	GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Pin = GPIO_PIN_8;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	/* remap CAN1 RX/TX to PB8/PB9 */
	AFIO->MAPR |= AFIO_MAPR_CAN_REMAP_1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* CAN1 interrupt Init */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_peripherials_init()
{
	hcan1.Instance = CAN1;
	hcan1.Init.Prescaler = 4;
	hcan1.Init.Mode = CAN_MODE_NORMAL;
	hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan1.Init.TimeSeg1 = CAN_BS1_15TQ;
	hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
	hcan1.Init.TimeTriggeredMode = DISABLE;
	hcan1.Init.AutoBusOff = DISABLE;
	hcan1.Init.AutoWakeUp = DISABLE;
	hcan1.Init.AutoRetransmission = DISABLE;
	hcan1.Init.ReceiveFifoLocked = DISABLE;
	hcan1.Init.TransmitFifoPriority = DISABLE;

	uint8_t can_status = HAL_CAN_Init(&hcan1);

	ASSERT_LOG_ERROR_G(can_status != HAL_OK, "CAN init ERROR with param: %d",
					   can_status);
	ASSERT_LOG_INFO_G(can_status == HAL_OK, "CAN init OK!");	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

CAN_HandleTypeDef *can_get_handle_ptr(void)
{
	return &hcan1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|