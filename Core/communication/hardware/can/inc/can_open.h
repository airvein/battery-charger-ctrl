#ifndef __CAN_OPEN_H
#define __CAN_OPEN_H

#include "stm32f1xx_hal.h"
#include "settings_app/communication/settings_can.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern CAN_HandleTypeDef hcan1;
extern CAN_TxHeaderTypeDef pHeader;
extern CAN_RxHeaderTypeDef pRxHeader;
extern uint32_t TxMailbox;
extern uint32_t a, r;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void can_hardware_init(void);
void can_filters_init(uint8_t ctrl_id, uint16_t msg_id_offset);
CAN_HandleTypeDef *can_get_handle_ptr(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __CAN_OPEN_H */
