#ifndef __PROCEDURE_H
#define __PROCEDURE_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define IDLE_ACTION                         -1
#define PROCEDURE_NO_CLBK                   NULL
#define PROC_MAX_ARGS_SIZE                  10

#define PROCEDURE_NO_ARGS                   NULL, 0
#define PROCEDURE_TEXT_ARGS_SIZE(text)      (strlen(text)+1)

#define ACTION_CALLBACKS(feas_cb, exec_cb, eval_cb, done_cb, err_cb) \
    ((struct action_callbacks_s)\
    {\
        .feasibility_check = feas_cb,\
        .execute = exec_cb,\
        .evaluate = eval_cb,\
        .done = done_cb,\
        .error = err_cb\
    })

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum 
{
    PROCEDURE_RETVAL_PENDING,
    PROCEDURE_RETVAL_DONE,
    PROCEDURE_RETVAL_ERROR

} procedure_retval_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    PROCEDURE_STATE_IDLE,
    PROCEDURE_STATE_EXECUTING,
    PROCEDURE_STATE_EVALUATING,
    PROCEDURE_STATE_DONE,
    PROCEDURE_STATE_ERROR,
    PROCEDURE_STATE_STARTED

} procedure_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    ACTION_PENDING,  //sprawdza czy moze sie wykonac
    ACTION_ACCEPTED, // zaakceptwanae
    ACTION_REJECTED,  //odrzucone
    ACTION_SKIPPED  //bledny parametr

} action_resp_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef action_resp_t (*feasibility_action_cb)(uint32_t*, uint8_t);

typedef procedure_retval_t (*operating_action_cb)(uint32_t*, uint8_t);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct action_callbacks_s
{
    feasibility_action_cb feasibility_check;
    operating_action_cb execute;
    operating_action_cb evaluate;
    operating_action_cb done;
    operating_action_cb error;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _procedure_s
{
    char *name;
    uint32_t id;
    bool is_triggered;

    procedure_state_t state;
    procedure_state_t prev_state;

    struct procedure_action_s 
    {
        char *name;

        feasibility_action_cb feasibility_check_cb;
        operating_action_cb execute_cb;
        operating_action_cb evaluate_cb;
        operating_action_cb done_cb;
        operating_action_cb error_cb;

        action_resp_t feasibility_resp;

        bool is_init;

    } **actions;
    
    uint16_t actions_num;

    uint32_t action_args[PROC_MAX_ARGS_SIZE];
    uint8_t action_args_size;

    int16_t order_action_id;
    int16_t execute_action_id;

    bool is_init;
    
} procedure_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t *procedure_create(void);

bool procedure_init(procedure_t *procedure, char *name, uint32_t id);

bool procedure_add_action(procedure_t *procedure, char *name, 
                          struct action_callbacks_s action_cbs);

bool procedure_trigger(procedure_t *procedure, int16_t procedure_action_id, 
                       uint32_t *arg, uint8_t arg_size);

int16_t procedure_get_action_id(procedure_t *procedure, char *action_name);

bool procedure_handler(procedure_t *procedure);

action_resp_t procedure_get_trigger_feasibility_resp(procedure_t *procedure);

bool procedure_is_args_action_not_empty(uint32_t* args, uint8_t args_size);

bool find_arg_on_procedure(uint32_t *args, uint8_t args_size,
                                           uint32_t search_arg);


#endif /* __PROCEDURE_H */
