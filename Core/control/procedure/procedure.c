#include "control/procedure/inc/procedure.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_procedure, "%s : %s", __FILE__, \
                                    __func__))

#define PROCEDURE_ARGS  procedure->action_args, procedure->action_args_size

#define ORDER_ACTION_FEASIBILITY_CB() \
            (get_order_procedure_action(procedure)->feasibility_check_cb( \
            PROCEDURE_ARGS))

#define EXECUTE_ACTION_CB() \
            (get_order_procedure_action(procedure)->execute_cb( \
            PROCEDURE_ARGS))

#define EVALUATE_ACTION_CB() \
            (get_order_procedure_action(procedure)->evaluate_cb( \
            PROCEDURE_ARGS))

#define DONE_ACTION_CB() \
            (get_order_procedure_action(procedure)->done_cb( \
            PROCEDURE_ARGS))

#define ERROR_ACTION_CB() \
            (get_order_procedure_action(procedure)->error_cb( \
            PROCEDURE_ARGS))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_INFO_CURRENT_PROCEDURE_STATE(state_str)\
    {\
        if(procedure->state != procedure->prev_state)\
        {\
            log_info_m(log_procedure,\
                    "Procedure [ %s ] action [ %s ] %s!",\
                    procedure->name,\
                    get_exec_procedure_action(procedure)->name, state_str);\
            procedure->prev_state = procedure->state;\
        }\
    }

#define LOG_WARNING_ERROR_PROCEDURE_STATE()\
    {\
        if(procedure->state != procedure->prev_state)\
        {\
            log_warning_m(log_procedure,\
                        "Procedure [ %s ] action [ %s ] error!",\
                        procedure->name,\
                        get_exec_procedure_action(procedure)->name);\
            procedure->prev_state = procedure->state;\
        }\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(procedure_t *procedure, char *name);

static bool is_procedure_init(procedure_t *procedure);

static bool is_correct_action_parameters(char *name, 
                                         struct action_callbacks_s action_cbs);

static bool is_action_init(struct procedure_action_s *action);

static void malloc_memory_for_precedure_action(procedure_t *procedure);

static bool is_possible_to_trigger_procedure(procedure_t *procedure, 
                                             uint16_t action_id);

static void procedure_untrigger(procedure_t *procedure);

static void procedure_triggered_handler(procedure_t *procedure);                                                   

static void procedure_operating_handler(procedure_t *procedure);

static struct procedure_action_s *get_order_procedure_action(
                                                    procedure_t *procedure);

static struct procedure_action_s *get_exec_procedure_action(
                                                    procedure_t *procedure); 

static bool is_trigger_action_feasibility_callback_null(procedure_t *procedure);

static bool is_execute_action_evaluate_callback_null(procedure_t *procedure);

static bool is_execute_action_done_callback_null(procedure_t *procedure);

static bool is_execute_action_error_callback_null(procedure_t *procedure);  

static void get_procedure_action_args(procedure_t *procedure, uint32_t *args,
                                      uint8_t args_size);

static bool is_dispossesion_procedure_action(procedure_t *procedure);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t *procedure_create(void)
{
    LOG_DEBUG_FUNC();
    return (procedure_t*)malloc(sizeof(procedure_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_init(procedure_t *procedure, char *name, uint32_t id)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(procedure, name) == false)
    {
        ASSERT_LOG_ERROR_M(name==NULL, log_procedure, 
                            "Init parameters incorrect!");
        ASSERT_LOG_ERROR_M(name!=NULL, log_procedure, 
                            "Init parameters incorrect for [ %s ] procedure!",
                            name);        
        return false;
    }

    int name_len = strlen(name);
    procedure->name = malloc(sizeof(char) * (name_len+1));
    strncpy(procedure->name, name, name_len);
    procedure->name[name_len] = '\0';
    
    procedure->id = id;

    procedure->actions = NULL;
    procedure->actions_num = 0;
    procedure->order_action_id = IDLE_ACTION;
    procedure->execute_action_id = IDLE_ACTION;

    memset(procedure->action_args, 0, sizeof(procedure->action_args));
    procedure->action_args_size = 0;

    procedure->state = PROCEDURE_STATE_IDLE;
    procedure->prev_state = PROCEDURE_STATE_STARTED;
    procedure->is_triggered = false;

    procedure->is_init = true;

    log_info_m(log_procedure, 
               "Procedure [ %s ] id [ %d ] initialized correctly!",
               procedure->name, procedure->id);

    return true; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_add_action(procedure_t *procedure, char *name, 
                          struct action_callbacks_s action_cbs)
{
    LOG_DEBUG_FUNC();
    if(is_procedure_init(procedure) == false)
    {
        log_error_m(log_procedure, "Procedure ptr is NULL");
        return false;
    }
    if(is_correct_action_parameters(name, action_cbs) == false)
    {
        log_error_m(log_procedure, "Incorrect action init parameters");
        return false;
    }

    procedure->actions_num++;

    malloc_memory_for_precedure_action(procedure);
    
    struct procedure_action_s *action;
    action = procedure->actions[procedure->actions_num-1]; 

    int name_len = strlen(name);
    action->name = malloc(sizeof(char)*(name_len+1));
    strncpy(action->name, name, name_len);
    action->name[strlen(name)] = '\0';

    action->feasibility_resp = ACTION_PENDING;

    action->feasibility_check_cb = action_cbs.feasibility_check;
    action->execute_cb = action_cbs.execute;
    action->evaluate_cb = action_cbs.evaluate;
    action->done_cb = action_cbs.done;
    action->error_cb = action_cbs.error;

    action->is_init = true;

    log_info_m(log_procedure, 
               "Add to procedure [ %s ] action [ %s ] correctly",
               procedure->name, action->name);

    return true;   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_trigger(procedure_t *procedure, int16_t procedure_action_id, 
                       uint32_t *arg, uint8_t arg_size)
{
    LOG_DEBUG_FUNC();
    if(is_possible_to_trigger_procedure(procedure, 
                                        procedure_action_id) == true)
    {
        procedure->is_triggered = true;
        procedure->order_action_id = procedure_action_id;
        get_order_procedure_action(procedure)->feasibility_resp = ACTION_PENDING;

        log_info_m(log_procedure, "Procedure [ %s ] action [ %s / %d ] triggered",
                    procedure->name, 
                    procedure->actions[procedure_action_id]->name,
                    procedure->order_action_id);

        get_procedure_action_args(procedure, arg, arg_size);

        return true;
    }
    else
    {
        //TODO: rozszerzyc warningi o informacje dlaczego
        ASSERT_LOG_WARNING_M(procedure!=NULL, log_procedure, 
                "Trigger procedure [ %s ] with action [ %d ] id cannot be done",
                procedure->name, procedure_action_id);
        ASSERT_LOG_WARNING_M(procedure==NULL, log_procedure, 
                "Trigger procedure [NULL] with action [ %d ] id cannot be done",
                procedure_action_id);

        return false;
    }
    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int16_t procedure_get_action_id(procedure_t *procedure, char *action_name)
{
    if(is_procedure_init(procedure) == false)
    {
        return IDLE_ACTION;
    }
    if(procedure->actions_num == 0)
    {
        return IDLE_ACTION;
    }

    for(int act_id = 0; act_id < procedure->actions_num; act_id++)
    {
        if(strcmp(procedure->actions[act_id]->name, action_name) == 0)
        {
            return act_id;
        }
    }
    log_warning_m(log_procedure, "No find action [ %s ] in [ %s ]",
                action_name, procedure->name);
    return IDLE_ACTION;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_handler(procedure_t *procedure)
{
    if(is_procedure_init(procedure) == false)
    {
        log_error_m(log_procedure, 
                    "Procedure structure pointer is NULL or is not init");
        return false;
    }

    procedure_triggered_handler(procedure);
    procedure_operating_handler(procedure);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

action_resp_t procedure_get_trigger_feasibility_resp(procedure_t *procedure)
{
    LOG_DEBUG_FUNC();
    if(procedure->is_triggered == false)
    {
        return get_order_procedure_action(procedure)->feasibility_resp;
    }
    else
    {
        return ACTION_PENDING;
    }   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_is_args_action_not_empty(uint32_t* args, uint8_t args_size)
{
    return args != NULL && args_size > 0;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool find_arg_on_procedure(uint32_t *args, uint8_t args_size,
                                           uint32_t search_arg)
{
    for(uint8_t i = 0; i < args_size; i++)
    {
        if(args[i] == search_arg)
        {
            return 1;
        }
    }
    return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(procedure_t *procedure, char *name)
{
    return procedure != NULL && name != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_procedure_init(procedure_t *procedure)
{
    return (procedure != NULL && procedure->is_init == true);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_action_parameters(char *name, 
                                         struct action_callbacks_s action_cbs)
{
    return (name != NULL && action_cbs.execute != PROCEDURE_NO_CLBK);
}     

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_action_init(struct procedure_action_s *action)
{
    return action != NULL && action->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void malloc_memory_for_precedure_action(procedure_t *procedure)
{
    LOG_DEBUG_FUNC();
    if(is_procedure_init(procedure) == true)
    {
        if(procedure->actions == NULL)
        {
            log_debug_m(log_procedure, 
                "Allocate memory 1st time for new action in procedure [ %s ]",
                procedure->name);

            procedure->actions = (struct procedure_action_s**)
                                  malloc(sizeof(struct procedure_action_s*));
            procedure->actions[0] = (struct procedure_action_s*)
                                    malloc(sizeof(struct procedure_action_s));
        }
        else
        {
            log_debug_m(log_procedure, 
                "Allocate memory for num [ %d ] action in procedure [ %s ]",
                procedure->actions_num,
                procedure->name);

            procedure->actions = realloc(procedure->actions,
                                        sizeof(struct procedure_action_s) * 
                                        procedure->actions_num);
            
            procedure->actions[procedure->actions_num-1] =  
                                    (struct procedure_action_s*)
                                    malloc(sizeof(struct procedure_action_s));
        }
    }
    else
    {
        log_error_m(log_procedure, 
                    "Memory allocating for not initialize procedure!");
    }
    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_to_trigger_procedure(procedure_t *procedure, 
                                             uint16_t action_id)
{
    return (is_procedure_init(procedure) == true )
            && procedure->is_triggered == false
            && action_id != IDLE_ACTION
            && action_id < procedure->actions_num
            && (action_id != procedure->execute_action_id
                || procedure->state == PROCEDURE_STATE_IDLE)
            && is_action_init(procedure->actions[action_id]);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void procedure_untrigger(procedure_t *procedure)
{
    procedure->is_triggered = false;
    log_info_m(log_procedure, 
                "Procedure [ %s ] untriggered!", procedure->name);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void procedure_triggered_handler(procedure_t *procedure)
{
    if(procedure->is_triggered == true)
    {       
        LOG_DEBUG_FUNC();
        if(is_trigger_action_feasibility_callback_null(procedure) == true)
        {
            log_info_m(log_procedure, 
            "Action [ %s ] from [ %s ] procedure ACCEPTED with NULL feas_cb ", 
            get_order_procedure_action(procedure)->name,
            procedure->name);

            get_order_procedure_action(procedure)->feasibility_resp 
                                                            = ACTION_ACCEPTED;

            ASSERT_LOG_INFO_M(is_dispossesion_procedure_action(procedure),
                log_procedure, 
                "Dispossession action [ %s => %s ] in procedure [ %s ]",
                get_exec_procedure_action(procedure)->name,
                get_order_procedure_action(procedure)->name,
                procedure->name);

            procedure->state = PROCEDURE_STATE_EXECUTING;
            procedure->execute_action_id = 
                                procedure->order_action_id;
            procedure_untrigger(procedure);
        }
        else
        {
            action_resp_t ret = ORDER_ACTION_FEASIBILITY_CB();

            if(ret != ACTION_PENDING)
            {
                get_order_procedure_action(procedure)->feasibility_resp = ret;
                
                if(ret == ACTION_ACCEPTED)
                {
                    log_info_m(log_procedure, 
                               "Action [ %s ] from [ %s ] procedure ACCEPTED ", 
                               get_order_procedure_action(procedure)->name,
                               procedure->name);

                    ASSERT_LOG_INFO_M(is_dispossesion_procedure_action(procedure),
                        log_procedure, 
                        "Dispossession action [ %s => %s ] in procedure [ %s ]",
                        get_exec_procedure_action(procedure)->name,
                        get_order_procedure_action(procedure)->name,
                        procedure->name);

                    procedure->state = PROCEDURE_STATE_EXECUTING;

                    procedure->execute_action_id = 
                                        procedure->order_action_id;
                }
                else
                {
                    log_warning_m(log_procedure,
                                "Action [ %s ] from [ %s ] procedure REJECTED", 
                                get_order_procedure_action(procedure)->name,
                                procedure->name);

                    procedure->state = PROCEDURE_STATE_IDLE;
                }
                procedure_untrigger(procedure);
            }
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void procedure_operating_handler(procedure_t *procedure)
{
    procedure_retval_t ret;

    switch (procedure->state)
    {
        case PROCEDURE_STATE_IDLE:                
            break;

        case PROCEDURE_STATE_EXECUTING:
            LOG_INFO_CURRENT_PROCEDURE_STATE("executing");

            ret = EXECUTE_ACTION_CB();

            if(ret != PROCEDURE_RETVAL_PENDING)
            {
                if(ret == PROCEDURE_RETVAL_DONE)
                {
                    procedure->state = PROCEDURE_STATE_EVALUATING;
                    break;
                }
                else
                {
                    procedure->state = PROCEDURE_STATE_ERROR;
                    break;                
                }
            }                                
            break;

        case PROCEDURE_STATE_EVALUATING:
            LOG_INFO_CURRENT_PROCEDURE_STATE("evaluating");

            if(is_execute_action_evaluate_callback_null(procedure) == true)
            {
                log_debug_m(log_procedure, 
                    "NULL eval_cb in [ %s ] action [ %s ] procedure",
                    get_exec_procedure_action(procedure)->name, 
                    procedure->name);

                procedure->state = PROCEDURE_STATE_DONE;
                break;
            }

            ret = EVALUATE_ACTION_CB();

            if(ret != PROCEDURE_RETVAL_PENDING)
            {
                if(ret == PROCEDURE_RETVAL_DONE)
                {
                    procedure->state = PROCEDURE_STATE_DONE;
                    break;
                }
                else
                {
                    procedure->state = PROCEDURE_STATE_ERROR;
                    break;                
                }
            }                                
            break;

        case PROCEDURE_STATE_DONE:
            LOG_INFO_CURRENT_PROCEDURE_STATE("done");
            
            if(is_execute_action_done_callback_null(procedure) == true)
            {
                log_debug_m(log_procedure, 
                    "NULL done_cb in [ %s ] action [ %s ] procedure",
                    get_exec_procedure_action(procedure)->name, 
                    procedure->name);

                procedure->state = PROCEDURE_STATE_IDLE;
                break;
            }

            DONE_ACTION_CB();
            
            procedure->order_action_id = IDLE_ACTION;
            procedure->execute_action_id = IDLE_ACTION;
            procedure->state = PROCEDURE_STATE_IDLE;
            break;


        case PROCEDURE_STATE_ERROR:
            LOG_WARNING_ERROR_PROCEDURE_STATE();

            if(is_execute_action_error_callback_null(procedure) == true)
            {
                log_debug_m(log_procedure, 
                        "NULL error_cb in [ %s ] action [ %s ] procedure",
                        get_exec_procedure_action(procedure)->name, 
                        procedure->name);

                procedure->state = PROCEDURE_STATE_IDLE;
                break;
            }

            ERROR_ACTION_CB();           
            
            procedure->order_action_id = IDLE_ACTION;
            procedure->execute_action_id = IDLE_ACTION;
            procedure->state = PROCEDURE_STATE_IDLE;
            break;                              
    
        default:
            log_error_m(log_procedure, 
            "Procedure [ %s ] state [ %d ] out of handler range! Fatal error!",
                    procedure->name,
                    procedure->state);
            break;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct procedure_action_s *get_order_procedure_action(
                                                    procedure_t *procedure)
{
    return procedure->actions[procedure->order_action_id];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static struct procedure_action_s *get_exec_procedure_action(
                                                        procedure_t *procedure)
{
    return procedure->actions[procedure->execute_action_id];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_trigger_action_feasibility_callback_null(procedure_t *procedure)
{
    return get_order_procedure_action(procedure)->feasibility_check_cb 
        == NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_execute_action_evaluate_callback_null(procedure_t *procedure)
{
    return get_exec_procedure_action(procedure)->evaluate_cb == NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_execute_action_done_callback_null(procedure_t *procedure)
{
    return get_exec_procedure_action(procedure)->done_cb == NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_execute_action_error_callback_null(procedure_t *procedure)
{
    return get_exec_procedure_action(procedure)->error_cb == NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void get_procedure_action_args(procedure_t *procedure, uint32_t *args,
                                      uint8_t args_size)
{
    if(args != NULL && args_size > 0)
    {
        memset(procedure->action_args, 0, sizeof(procedure->action_args));
        for(int i = 0; i < args_size; i++)
        {
            procedure->action_args[i] = args[i];
        }
        procedure->action_args_size = args_size;

        log_debug_m(log_procedure, 
                "Copy [ %d ] args for [ %s ] action in [ %s ] procedure",
                args_size,
                get_order_procedure_action(procedure)->name,
                procedure->name);
    }
    else
    {
        log_debug_m(log_procedure, 
                "No args for [ %s ] action in [ %s ] procedure",
                get_order_procedure_action(procedure)->name,
                procedure->name);

        procedure->action_args_size = 0;
        memset(procedure->action_args, 0, sizeof(procedure->action_args));
    }    
}                                      

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_dispossesion_procedure_action(procedure_t *procedure)
{
    return (procedure->execute_action_id != IDLE_ACTION
            && procedure->order_action_id != procedure->execute_action_id);    
}