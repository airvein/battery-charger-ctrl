#include "control/cmd_procedure/inc/cmd_procedure_ctrl.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_cmd_procedure_ctrl, "%s : %s",  \
                            __FILE__, __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(cmd_procedure_ctrl_t *cmd_ctrl,
                                        cmd_procedure_t **cmd_array,
                                        uint16_t cmd_array_size);

static bool is_cmd_ctrl_init(cmd_procedure_ctrl_t *cmd_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

cmd_procedure_ctrl_t *cmd_procedure_ctrl_create(void)
{
    return (cmd_procedure_ctrl_t*)malloc(sizeof(cmd_procedure_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_ctrl_init(cmd_procedure_ctrl_t *cmd_ctrl,
                             cmd_procedure_t **cmd_array, 
                             uint16_t cmd_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(cmd_ctrl, cmd_array, cmd_array_size) 
                                                                    == false)
    {
        log_error_m(log_cmd_procedure_ctrl,
                    "Incorrect init parameters!");
        return false;
    }

    cmd_ctrl->cmd_aray = cmd_array;
    cmd_ctrl->cmd_num = cmd_array_size;

    cmd_ctrl->is_init = true;

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cmd_procedure_ctrl_handler(cmd_procedure_ctrl_t *cmd_ctrl)
{
    if(is_cmd_ctrl_init(cmd_ctrl) == true)
    {
        for(int cmd_id = 0; cmd_id < cmd_ctrl->cmd_num; cmd_id++)
        {
            cmd_procedure_handler(cmd_ctrl->cmd_aray[cmd_id]);
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cmd_procedure_ctrL_print_commands(cmd_procedure_ctrl_t *cmd_ctrl)
{
    if(is_cmd_ctrl_init(cmd_ctrl) == false)
    {
        log_warning_m(log_cmd_procedure_ctrl, "Can't print commands!");
        return ;
    }

    log_info_m(log_controller, "Available service commands:");

    for(int cmd_id = 0; cmd_id < cmd_ctrl->cmd_num; cmd_id++)
    {
        cmd_procedure_print(cmd_ctrl->cmd_aray[cmd_id]);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(cmd_procedure_ctrl_t *cmd_ctrl,
                                        cmd_procedure_t **cmd_array,
                                        uint16_t cmd_array_size)
{
    return cmd_ctrl != NULL && cmd_array != NULL && cmd_array_size > 0;
}                                        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_cmd_ctrl_init(cmd_procedure_ctrl_t *cmd_ctrl)
{
    return cmd_ctrl != NULL && cmd_ctrl->is_init == true;
}
