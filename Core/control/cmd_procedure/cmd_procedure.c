#include "inc/cmd_procedure.h"
#include "debug/log_modules/log_modules.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_cmd_procedure, "%s : %s", __FILE__,\
                                    __func__))

#define RECV_PARAM_CHAR_ARR_LEN     255
#define NO_ACTION                    -1

#define SEPARATOR                   ','

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(cmd_procedure_t *cmd, 
                                       cmd_parameter_type_t param_type, 
                                       procedure_t *procedure, char *name);

static bool is_correct_param_type(cmd_parameter_type_t param_type);

static bool is_cmd_procedure_init(cmd_procedure_t *cmd);

static bool is_ch_arr_parameter_not_null(char *parameter);

static void malloc_memory_for_link_parameter(cmd_procedure_t *cmd);

static bool is_equal_parameter_char_array(char *inc_param, char *link_param);

static int16_t find_action_id_from_param_char_array(cmd_procedure_t *cmd, 
                                                    char *inc_param);

static int16_t find_action_id_from_param_u16(cmd_procedure_t *cmd, 
                                            uint16_t inc_param);

static bool cmd_call(cmd_procedure_t *cmd, int16_t action_id, uint32_t *arg, 
                     uint8_t arg_size);

static bool is_set_cmd_parameter_type_cmd(cmd_procedure_t *cmd, 
                                          cmd_parameter_type_t param_type);

static bool is_possible_to_call_cmd(cmd_procedure_t *cmd);

static bool is_possible_call_char_array_cmd(cmd_procedure_t *cmd, 
                                            char *parameter);

static bool is_possible_call_u16_cmd(cmd_procedure_t *cmd);

static void pass_parameter(extracted_variables_t *ev, char *parameter);

static void find_number_of_args(extracted_variables_t *ev);

static void find_separator_pos_on_parameter(extracted_variables_t *ev);

static void extract_cmd_from_parameter(extracted_variables_t *ev);

static void extract_args_from_parameter(extracted_variables_t *ev);

static bool check_type(char arg);

static uint8_t check_arg_size(extracted_variables_t *ev, uint8_t num);

static void print_command_actions(cmd_procedure_t *cmd);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static char *param_type_str[] = 
{
    "CHAR_ARR", "U16"
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static char *action_resp_str[] = 
{
    "PENDING", "ACCEPTED", "REJECTED", "SKIPPED"
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

cmd_procedure_t *cmd_procedure_create(void)
{
    return (cmd_procedure_t*)malloc(sizeof(cmd_procedure_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_init(cmd_procedure_t *cmd, cmd_parameter_type_t param_type,
                        procedure_t *procedure, char *name)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(cmd, param_type, procedure, name) == false)
    {
        ASSERT_LOG_ERROR_M(name != NULL, log_cmd_procedure, 
                            "Init parameters procedure [ %s ] incorrect",
                            name);
        ASSERT_LOG_ERROR_M(name == NULL, log_cmd_procedure, 
                            "Init parameters procedure incorrect");
        return false;
    }
    log_info_m(log_cmd_procedure, 
              "Initializing cmd [ %s ] for [ %s ] procedure (param_type %s)",
               name, procedure->name, param_type_str[param_type]);

    cmd->name = malloc(sizeof(char) * (strlen(name)+1));
    strcpy(cmd->name, name);

    cmd->parameter_type = param_type;
    cmd->parameter_arr = NULL;
    
    cmd->procedure_ptr = procedure;

    cmd->action_id_arr = NULL;
    cmd->link_action_num = 0;    

    cmd->state = CMD_STATE_IDLE;
    cmd->send_response_cb = NULL;
    cmd->order_num = 0;

    cmd->is_init = true;

    log_info_m(log_cmd_procedure, 
               "Init [ %s ] cmd correctly!",
               cmd->name);

    return true;
}         

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_register_send_response_cb(cmd_procedure_t *cmd,
                                            cmd_send_response_cb send_resp_cb)
{
    LOG_DEBUG_FUNC();
    if(is_cmd_procedure_init(cmd) == false)
    {
        log_error_m(log_cmd_procedure, "CMD is not init correctly");
        return false;
    }
    if(send_resp_cb == NULL)
    {
        log_error_m(log_cmd_procedure, "send_resp_cb is NULL for [ %s ] cmd",
                    cmd->name);
        return false;
    }

    cmd->send_response_cb = send_resp_cb;
    log_info_m(log_cmd_procedure,
                "Response sender callback register succesfull [ %s ]",
                cmd->name);
    return true;   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_link_parameter_ch_arr_to_action_id(cmd_procedure_t *cmd, 
                                                      char *parameter, 
                                                      uint16_t action_id)
{
    LOG_DEBUG_FUNC();
    if(is_cmd_procedure_init(cmd) == false)
    {
        log_error_m(log_cmd_procedure, "CMD is not init correctly,"
                    " link parameter with action_id [ %d ] error",
                    action_id);
        return false;
    }
    if(is_set_cmd_parameter_type_cmd(cmd, CMD_PARAM_CHAR_ARRAY) == false)
    {
        log_error_m(log_cmd_procedure, 
                    "Try link cmd [ %s ] with not correct parameter type!",
                    cmd->name);
        return false;
    }
    if(is_ch_arr_parameter_not_null(parameter) == false)
    {
        log_error_m(log_cmd_procedure, 
                    "Link parameter ch_arr for cmd [ %s ] is null",
                    cmd->name);                    
        return false;
    }

    cmd->link_action_num++;

    malloc_memory_for_link_parameter(cmd);
    cmd->action_id_arr[cmd->link_action_num-1] = action_id;

    int param_len = strlen((char*)parameter) + 1;
    cmd->parameter_arr[cmd->link_action_num-1]._char_array = 
                                                malloc(sizeof(char)*param_len);
    strcpy(cmd->parameter_arr[cmd->link_action_num-1]._char_array, parameter);       

    log_info_m(log_cmd_procedure, 
                "Link [ %s ] charr_parameter to [ %d ] action_id" 
                " in cmd_procedure [ %s ]",
                cmd->parameter_arr[cmd->link_action_num-1]._char_array,
                action_id, cmd->name);
    return true;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_link_parameter_u16_to_action_id(cmd_procedure_t *cmd, 
                                                   uint16_t parameter, 
                                                   uint16_t action_id)
{
    LOG_DEBUG_FUNC();
    if(is_cmd_procedure_init(cmd) == false)
    {
        log_error_m(log_cmd_procedure, "CMD is not init correctly,"
                    " link parameter with action_id [ %d ] error",
                    action_id);
        return false;
    }
    if(is_set_cmd_parameter_type_cmd(cmd, CMD_PARAM_U16) == false)
    {
        log_error_m(log_cmd_procedure, 
                    "Try link cmd [ %s ] with not correct parameter type!",
                    cmd->name);
        return false;
    }

    cmd->link_action_num++;

    malloc_memory_for_link_parameter(cmd);
    cmd->action_id_arr[cmd->link_action_num-1] = action_id;

    cmd->parameter_arr[cmd->link_action_num-1]._uint16 = parameter;

    log_info_m(log_cmd_procedure, 
                "Link [ %s ] u16_parameter to [ %d ] action_id" 
                " in cmd_procedure [ %s ]",
                cmd->parameter_arr[cmd->link_action_num-1]._uint16,
                action_id, cmd->name);
    return true;
}                                                          

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_call_char_arr(cmd_procedure_t *cmd, char *parameter)
{
    LOG_DEBUG_FUNC();
    if(is_possible_call_char_array_cmd(cmd, parameter) == false)
    {
        return false;
    }

    extracted_variables_t ev;
    extracted_variables_t *ev_ptr = &ev;

    pass_parameter(ev_ptr, parameter);
    find_number_of_args(ev_ptr);

    if(ev_ptr->numbers_of_args == 0)
    {
        int16_t action_id = find_action_id_from_param_char_array(cmd,parameter);
        return cmd_call(cmd, action_id, NULL, ev.numbers_of_args);
    }

    find_separator_pos_on_parameter(ev_ptr);
    extract_cmd_from_parameter(ev_ptr);
    extract_args_from_parameter(ev_ptr);

    int16_t action_id = find_action_id_from_param_char_array(cmd, ev.cmd);
    return cmd_call(cmd, action_id, ev.arg, ev.numbers_of_args);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cmd_procedure_call_u16(cmd_procedure_t *cmd, uint16_t parameter, 
                            uint32_t *arg, uint8_t arg_size)
{
    LOG_DEBUG_FUNC();
    if(is_possible_call_u16_cmd(cmd) == false)
    {
        return false;
    }

    int16_t action_id = find_action_id_from_param_u16(cmd, parameter);

    return cmd_call(cmd, action_id, arg, arg_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cmd_procedure_handler(cmd_procedure_t *cmd)
{
    if(is_cmd_procedure_init(cmd) == false)
    {
        return ;
    }
    
    switch (cmd->state)
    {
        case CMD_STATE_IDLE:
            break;

        case CMD_STATE_WAIT_FOR_RESP:
            if(cmd->send_response_cb == NULL)
            {
                log_warning_m(log_cmd_procedure, 
                            "No sender response callback in [ %s ]",
                            cmd->name);
                cmd->state = CMD_STATE_IDLE;
                break;
            }

            action_resp_t resp = 
                procedure_get_trigger_feasibility_resp(cmd->procedure_ptr);

            if(resp != ACTION_PENDING)
            {
                log_info_m(log_cmd_procedure, "Sending [ %s ] response [ %s ]",
                            cmd->name, action_resp_str[resp]);
                            
                cmd->send_response_cb(resp, cmd->order_num);
                cmd->state = CMD_STATE_IDLE;
            }
            break;
    
        default:
            log_error_m(log_cmd_procedure,
                        "[ %s ] state is out of handler range [ %d ]!",
                        cmd->name, cmd->state);
            break;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cmd_procedure_print(cmd_procedure_t *cmd)
{
    if(is_cmd_procedure_init(cmd) == false)
    {
        return ;
    }

    log_info_m(log_cmd_procedure, 
            "command_name { %s } link action num { %d }", cmd->name,
                                                        cmd->link_action_num);
    print_command_actions(cmd);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(cmd_procedure_t *cmd, 
                                       cmd_parameter_type_t param_type, 
                                       procedure_t *procedure, char *name)
{
    return cmd != NULL && is_correct_param_type(param_type) == true
           && procedure != NULL && procedure->is_init == true
           && name != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_param_type(cmd_parameter_type_t param_type)
{
    return param_type >= CMD_PARAM_CHAR_ARRAY && param_type <= CMD_PARAM_U16;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_cmd_procedure_init(cmd_procedure_t *cmd)
{
    return cmd != NULL && cmd->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_ch_arr_parameter_not_null(char *parameter)
{
    return parameter != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void malloc_memory_for_link_parameter(cmd_procedure_t *cmd)
{
    LOG_DEBUG_FUNC();
    if(cmd->link_action_num < 2)
    {
        cmd->action_id_arr = malloc(sizeof(uint16_t));

        cmd->parameter_arr = malloc(sizeof(union parameter));
    }
    else
    {
        cmd->action_id_arr = realloc(cmd->action_id_arr, 
                                sizeof(uint16_t)*cmd->link_action_num);

        cmd->parameter_arr = realloc(cmd->parameter_arr, 
                                sizeof(union parameter)*cmd->link_action_num);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_equal_parameter_char_array(char *inc_param, char *link_param)
{
    return strcmp(inc_param, link_param) == 0;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static int16_t find_action_id_from_param_char_array(cmd_procedure_t *cmd, 
                                                    char *inc_param)
{
    LOG_DEBUG_FUNC();
    for(int param_id = 0; param_id < cmd->link_action_num; param_id++)
    {   
        char *param = cmd->parameter_arr[param_id]._char_array;
        if(is_equal_parameter_char_array(inc_param, param) == true)
        {
            log_debug_m(log_cmd_procedure,
                        "Find action_id [ %d ] with link [ %s ] " 
                        "inc_param_ch_arr in [ %s ] cmd_procedure",
                        param_id, inc_param, cmd->name);
            return cmd->action_id_arr[param_id];
        }
    }
    log_warning_m(log_cmd_procedure,
                  "No action_id with link [ %s ] inc_param"
                  " in [ %s ] cmd_procedure",
                  inc_param, cmd->name);

    return NO_ACTION;
}                                        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static int16_t find_action_id_from_param_u16(cmd_procedure_t *cmd, 
                                            uint16_t inc_param)
{
    LOG_DEBUG_FUNC();
    for(int param_id = 0; param_id < cmd->link_action_num; param_id++)
    {
        if(inc_param == cmd->parameter_arr[param_id]._uint16)
        {
            log_debug_m(log_cmd_procedure,
                        "Find action_id [ %d ] with link [ %d ] inc_param_u16"
                        " in [ %s ] cmd_procedure",
                        param_id, inc_param, cmd->name);
            return cmd->action_id_arr[param_id];
        }
    }
    log_warning_m(log_cmd_procedure,
                  "No action_id with link [ %ds ] inc_param_u16"
                  " in [ %s ] cmd_procedure",
                  inc_param, cmd->name);

    return NO_ACTION;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool cmd_call(cmd_procedure_t *cmd, int16_t action_id, uint32_t *arg, 
                     uint8_t arg_size)
{
    cmd->order_num++;

    if(action_id == NO_ACTION)
    {
        log_warning_m(log_cmd_procedure, 
                "[ %s ] cmd : action skipped (not found)!",
                cmd->name);

        if(cmd->send_response_cb == NULL)
        {
            log_warning_m(log_cmd_procedure, 
                        "No sender response callback in [ %s ]",
                        cmd->name);
            cmd->state = CMD_STATE_IDLE;
        }
        else
        {
            cmd->send_response_cb(ACTION_SKIPPED, cmd->order_num);
            log_info_m(log_cmd_procedure, 
                    "Send response in [ %s ]",
                    cmd->name);
        }
        return false;
    }
    
    log_info_m(log_cmd_procedure,
                "[ %s ] trigger procedure with [ %d ] action_id",
                cmd->name, action_id);

    procedure_trigger(cmd->procedure_ptr, action_id, arg, arg_size);
    cmd->state = CMD_STATE_WAIT_FOR_RESP;
    
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_set_cmd_parameter_type_cmd(cmd_procedure_t *cmd, 
                                          cmd_parameter_type_t param_type)
{
    return cmd->parameter_type == param_type;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_to_call_cmd(cmd_procedure_t *cmd)
{
    return cmd->state == CMD_STATE_IDLE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_call_char_array_cmd(cmd_procedure_t *cmd, 
                                            char *parameter)
{
    if(is_cmd_procedure_init(cmd) == false)
    {
        log_error_m(log_cmd_procedure, "Cmd not initialized correctly!");
        return false;
    }
    if(is_set_cmd_parameter_type_cmd(cmd, CMD_PARAM_CHAR_ARRAY) == false)
    {
        log_error_m(log_cmd_procedure, 
                    "Try call cmd [ %s ] with not correct parameter type!",
                    cmd->name);
        return false;
    }
    if(is_ch_arr_parameter_not_null(parameter) == false)
    {
        log_warning_m(log_cmd_procedure, 
                      "Try call cmd [ %s ] with NULL parameter",
                      cmd->name);
        return false;
    }
    if(is_possible_to_call_cmd(cmd) == false)
    {
        log_warning_m(log_cmd_procedure,
                      "Command [ %s ] is waiting already for resp!",
                      cmd->name);
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_call_u16_cmd(cmd_procedure_t *cmd)
{
    if(is_cmd_procedure_init(cmd) == false)
    {
        log_error_m(log_cmd_procedure, "Cmd not initialized correctly!");
        return false;
    }
    if(is_set_cmd_parameter_type_cmd(cmd, CMD_PARAM_CHAR_ARRAY) == false)
    {
        log_error_m(log_cmd_procedure, 
                    "Try call cmd [ %s ] with not correct parameter type!",
                    cmd->name);
        return false;
    }
    if(is_possible_to_call_cmd(cmd) == false)
    {
        log_warning_m(log_cmd_procedure,
                      "Command [ %s ] is waiting already for resp!",
                      cmd->name);
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void pass_parameter(extracted_variables_t *ev, char *parameter)
{
    strcpy(ev->parameter, parameter);
    ev->parameter_len = strlen(ev->parameter);
    log_debug_m(log_cmd_procedure, "parameter: [%s] copy with len [%d]",
                                    ev->parameter, ev->parameter_len);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void find_number_of_args(extracted_variables_t *ev)
{
    ev->numbers_of_args = 0;
    
    for(uint8_t i = 0; i < ev->parameter_len; i++)
    {
        if(ev->parameter[i] == SEPARATOR)
        {
            ev->numbers_of_args++;  
        }
    }
    log_debug_m(log_cmd_procedure, "found [%d] args!", ev->numbers_of_args);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void find_separator_pos_on_parameter(extracted_variables_t *ev)
{
    uint8_t separator_pos_num = 0;

    for(uint8_t i = 0; i < ev->parameter_len; i++)
    {
        if(ev->parameter[i] == SEPARATOR)
        {
            ev->separator_positions[separator_pos_num] = i;
            log_debug_m(log_cmd_procedure, "Separator(%d) pos = [%d]",
                        separator_pos_num,
                        ev->separator_positions[separator_pos_num]);
            separator_pos_num++;
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void extract_cmd_from_parameter(extracted_variables_t *ev)
{   
    ev->cmd_len = ev->separator_positions[0] + 1;

    for(int i = 0; i < (ev->cmd_len - 1); i++)
    {
        ev->cmd[i] = ev->parameter[i];
    }
    ev->cmd[ev->cmd_len- 1] = '\0';

    log_debug_m(log_cmd_procedure, "Extracted cmd from parameter = [%s]", 
                                    ev->cmd);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void extract_args_from_parameter(extracted_variables_t *ev)
{   
    for(uint8_t i = 0; i < ev->numbers_of_args; i++)
    {
        uint8_t extracting_arg_size = 0;
        bool is_char = 0;

        is_char = check_type(ev->parameter[ev->separator_positions[i]+1]);

        extracting_arg_size = check_arg_size(ev, i);
        log_debug_m(log_cmd_procedure, "Arg(%d) size = [%d]",
                    i, extracting_arg_size);
        
        char extracted_arg[extracting_arg_size + 1];

        for(int j = 0; j < extracting_arg_size; j++)
        {
            extracted_arg[j] = 
                ev->parameter[ev->separator_positions[i] + j + 1];
        }
        extracted_arg[extracting_arg_size] = '\0';
        
        if(is_char == 0)
        {
            ev->arg[i] = atoi(extracted_arg);
            log_info_m(log_cmd_procedure,
                "Extracted arg from cmd [ %d ] = [ %d ]", i, ev->arg[i]);
        }
        else
        {
            ev->arg[i] = (uint8_t)extracted_arg[0];
            log_info_m(log_cmd_procedure,
                "Extracted arg from cmd [ %d ] = [ %c ]", i, ev->arg[i]);
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool check_type(char arg)
{
    return isdigit(arg) == 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t check_arg_size(extracted_variables_t *ev, uint8_t num)
{

    uint8_t extracting_arg_size = 0;
    uint8_t parameter_len = strlen(ev->parameter);

    if((ev->numbers_of_args - 1) == num)
    {
        return extracting_arg_size =
         (parameter_len - ev->separator_positions[num] - 1);
        
    }
    else
    {
        return extracting_arg_size =
         (ev->separator_positions[num + 1] - ev->separator_positions[num] - 1);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void print_command_actions(cmd_procedure_t *cmd)
{
    if(cmd->link_action_num > 0)
    {
        for(int act_id = 0; act_id < cmd->link_action_num; act_id++)
        {
            if(cmd->parameter_type == CMD_PARAM_CHAR_ARRAY)
            {
                log_info_m(log_cmd_procedure, 
                        "\tcmd_action_id { %d } name { %s }",
                        act_id, cmd->parameter_arr[act_id]._char_array);
            }
            else if(cmd->parameter_type == CMD_PARAM_U16)
            {
                log_info_m(log_cmd_procedure, 
                        "\tcmd_action_id { %d } parameter_id { %d }",
                        act_id, cmd->parameter_arr[act_id]._uint16);
            }
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//      P A R A M E T E R S   T Y P E   H A N D L E R S   C A L L B A C K S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// bool cmd_procedure_link_parameter_to_action_id(cmd_procedure_t *cmd, 
//                                                 void *parameter, 
//                                                 uint16_t action_id)
// {
//     LOG_DEBUG_FUNC();
//     if(is_cmd_procedure_init(cmd) == false)
//     {
//         log_error_m(log_cmd_procedure, "CMD is not init correctly,"
//                     " link parameter with action_id [ %d ] error",
//                     action_id);
//         return false;
//     }
//     if(is_parameter_not_null(parameter) == false)
//     {
//         log_error_m(log_cmd_procedure, "Link parameter for cmd [ %s ] is null",
//                     cmd->name);                    
//         return false;
//     }

//     cmd->link_link_action_num++;

//     malloc_memory_for_link_parameter(cmd);
//     cmd->action_id_arr[cmd->link_link_action_num-1] = action_id;

//     if(cmd->parameter_type == CMD_PARAM_CHAR_ARRAY)
//     {
        
//         int param_len = strlen((char*)parameter) + 1;
//         cmd->parameter_arr[cmd->link_link_action_num-1]._char_array = malloc(sizeof(char)*param_len);
//         strcpy(cmd->parameter_arr[cmd->link_link_action_num-1]._char_array, (char*)parameter);       

//         log_debug_m(log_cmd_procedure, 
//                     "Link [ %s ] charr_parameter to [ %d ] action_id" 
//                     " in cmd_procedure [ %s ]",
//                     cmd->parameter_arr[cmd->link_link_action_num-1]._char_array,
//                     action_id, cmd->name);
//     }
//     else if(cmd->parameter_type == CMD_PARAM_U16)
//     {
//         cmd->parameter_arr[cmd->link_link_action_num-1]._uint16 = *(uint16_t*)parameter;

//         log_debug_m(log_cmd_procedure, 
//                     "Link [ %s ] u16_parameter to [ %d ] action_id" 
//                     " in cmd_procedure [ %s ]",
//                     cmd->parameter_arr[cmd->link_action_num-1]._uint16,
//                     action_id, cmd->name);
//     }
//     else
//     {
//         log_error_m(log_cmd_procedure, "Link parameter error, cmd_param_type!");
//         return false;
//     }    

//     log_info_m(log_cmd_procedure, 
//                 "Link parameter num [ %d ] in [ %s ] correctly!",
//                 cmd->link_action_num-1, cmd->name);
//     return true;
// }         

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static bool set_incoming_parameter_to_set_type(cmd_procedure_t *cmd, 
//                                                void *receive_parameter)
// {
//     LOG_DEBUG_FUNC();
//     if(is_parameter_not_null(receive_parameter) == false)
//     {
//         log_error_m(log_cmd_procedure, 
//                     "Set incoming parameter [NULL] to data type for [ %s ]",
//                     cmd->name);
//         return false;
//     }

//     switch (cmd->parameter_type)
//     {
//         case CMD_PARAM_CHAR_ARRAY:
//         	;
//             int recv_len = strlen((char*)receive_parameter);
//             strncpy(cmd->recv_parameter._char_array, (char*)receive_parameter,
//                     recv_len+1);
//             cmd->recv_parameter._char_array[recv_len] = '\0';
//             log_debug_m(log_cmd_procedure, 
//                         "Set incoming parmeter [ch_arr = %s] to cmd [ %s ]",
//                         cmd->recv_parameter._char_array, cmd->name);            
//             break;
//         case CMD_PARAM_U8:
//             cmd->recv_parameter._uint8 = *(uint8_t*)receive_parameter;
//             log_debug_m(log_cmd_procedure, 
//                         "Set incoming parmeter [u8 = %d] to cmd [ %s ]",
//                         cmd->recv_parameter._uint8, cmd->name);
//             break;
//         case CMD_PARAM_U16:
//             cmd->recv_parameter._uint16 = *(uint16_t*)receive_parameter;
//             log_debug_m(log_cmd_procedure, 
//                         "Set incoming parmeter [u16 = %d] to cmd [ %s ]",
//                         cmd->recv_parameter._uint16, cmd->name);
//             break;
//         case CMD_PARAM_U32:
//             cmd->recv_parameter._uint32 = *(uint32_t*)receive_parameter;
//             log_debug_m(log_cmd_procedure, 
//                         "Set incoming parmeter [u32 = %d] to cmd [ %s ]",
//                         cmd->recv_parameter._uint32, cmd->name);
//             break;

//         default:
//             break;
//     }
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static int16_t find_action_id_from_parameter(cmd_procedure_t *cmd, 
//                                              void *parameter)
// {
//     if(parameter != NULL)
//     {
//         if(find_param_handlers_cb[cmd->parameter_type] != NULL)
//         {
//             return (*find_param_handlers_cb[cmd->parameter_type])(cmd, parameter);
//         }
//         else
//         {
//             log_error_m(log_cmd_procedure,
//                         "No find param [ %d ] handler cb", 
//                         cmd->parameter_type);
//             return NO_PARAM;
//         }
//     }
//     else
//     {
//         return NO_PARAM;
//     }
// }
