#ifndef __CMD_PROCEDURE_CTRL_H
#define __CMD_PROCEDURE_CTRL_H

#include "control/cmd_procedure/inc/cmd_procedure.h"

#include <stdint.h>
#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _cmd_procedure_ctrl_s
{
    cmd_procedure_t **cmd_aray;
    uint16_t        cmd_num;

    bool is_init;

} cmd_procedure_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

cmd_procedure_ctrl_t *cmd_procedure_ctrl_create(void);

bool cmd_procedure_ctrl_init(cmd_procedure_ctrl_t *cmd_ctrl,
                             cmd_procedure_t **cmd_array,
                             uint16_t cmd_array_size);

void cmd_procedure_ctrl_handler(cmd_procedure_ctrl_t *cmd_ctrl);

void cmd_procedure_ctrL_print_commands(cmd_procedure_ctrl_t *cmd_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CMD_PROCEDURE_CTRL_H */
