#ifndef __CMD_PROCEDURE_H
#define __CMD_PROCEDURE_H

#include "control/procedure/inc/procedure.h"

#include <stdint.h>
#include <stdbool.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef void (*cmd_send_response_cb)(action_resp_t, uint32_t);

typedef enum _cmd_parameter_type_e
{
    CMD_PARAM_CHAR_ARRAY,
    CMD_PARAM_U16

} cmd_parameter_type_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _cmd_state_e
{
    CMD_STATE_IDLE,
    CMD_STATE_WAIT_FOR_RESP,
    CMD_STATE_SEND_RESP

} cmd_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _extracted_variables_s
{
    char parameter[100];
    uint8_t parameter_len;
    uint8_t numbers_of_args;
    uint8_t separator_positions[10];
    uint8_t cmd_len;
    char cmd[20];
    uint16_t cmd_u16;
    uint32_t arg[10];

} extracted_variables_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _cmd_procedure_s
{
    char *name;
    cmd_parameter_type_t parameter_type;

    union parameter
    {
        char        *_char_array;
        uint16_t    _uint16;

    } *parameter_arr;//, recv_parameter;

    uint32_t order_num;

    procedure_t *procedure_ptr;
    uint16_t *action_id_arr;
    uint8_t link_action_num;

    cmd_state_t state;

    cmd_send_response_cb send_response_cb;

    bool is_init;

} cmd_procedure_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

cmd_procedure_t *cmd_procedure_create(void);

bool cmd_procedure_init(cmd_procedure_t *cmd, cmd_parameter_type_t param_type, 
                        procedure_t *procedure, char *name);  

bool cmd_procedure_register_send_response_cb(cmd_procedure_t *cmd, 
                                            cmd_send_response_cb send_resp_cb);

// bool cmd_procedure_link_parameter_to_action_id(cmd_procedure_t *cmd, 
//                                                 void *parameter, 
//                                                 uint16_t action_id);

bool cmd_procedure_link_parameter_ch_arr_to_action_id(cmd_procedure_t *cmd, 
                                                      char *parameter, 
                                                      uint16_t action_id);

bool cmd_procedure_link_parameter_u16_to_action_id(cmd_procedure_t *cmd, 
                                                   uint16_t parameter, 
                                                   uint16_t action_id);                                                    

bool cmd_procedure_call_char_arr(cmd_procedure_t *cmd, char *parameter);

bool cmd_procedure_call_u16(cmd_procedure_t *cmd, uint16_t parameter, 
                            uint32_t *arg, uint8_t arg_size);                            

void cmd_procedure_handler(cmd_procedure_t *cmd);    

void cmd_procedure_print(cmd_procedure_t *cmd);

// cmd_procedure_link_parameter_to_action_id(roof_procedure, "open", ROOF_OPEN_ACTION_ID);

#endif /* __CMD_PROCEDURE_H */