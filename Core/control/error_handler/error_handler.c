#include "inc/error_handler.h"
#include "debug/log_modules/log_modules.h"
#include "debug/diagnostic/inc/notify_signal.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ERROR_OCCURRED              1

#define LED_PORT                    GPIOC
#define LED_PIN                     GPIO_PIN_0

#define ERR_NOTIFY_QUICK_SIG_NUM    2
#define ERR_NOTIFY_SLOW_SIG_NUM     1


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static bool connection_flag = false;
static notify_signal_t notify_error = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_hard_error_init(void);

static void led_init(void);
static void led_on(void);
static void led_off(void);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void error_handler(void)
{
    log_error_m(log_controller, "HardError occurred! Error handler started!");
    log_error_m(log_controller, " = = = = = = = = = = = = = = = = = = = = ");
    
    notify_hard_error_init();

    while(ERROR_OCCURRED)
    {
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_hard_error_init(void)
{
    notify_signal_init(&notify_error, ERR_NOTIFY_QUICK_SIG_NUM, 
                        ERR_NOTIFY_SLOW_SIG_NUM);

    notify_signal_register_signal_on_cb(led_on);
    notify_signal_register_signal_off_cb(led_off);
    
    led_init();
    notify_signal_enable(&notify_error);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void led_init(void)
{
    GPIO_InitTypeDef gpio;
    gpio.Pin = LED_PIN;
    gpio.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(LED_PORT, &gpio);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void led_on(void)
{
    HAL_GPIO_WritePin(LED_PORT, LED_PIN, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void led_off(void)
{
    HAL_GPIO_WritePin(LED_PORT, LED_PIN, 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|