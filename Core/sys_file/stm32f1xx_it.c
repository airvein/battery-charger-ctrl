#include "app/inc/main.h"
#include "inc/stm32f1xx_it.h"
#include "debug/log_modules/log_modules.h"
#include "communication_interface/canbus/inc/canbus.h"
#include "app_time/inc/app_timeouts.h"
#include "app_time/inc/app_time_events.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern CAN_HandleTypeDef hcan1;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void NMI_Handler(void)
{

    while (1)
    {
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void HardFault_Handler(void)
{
    log_error_g("System HardFault!");
    while (1)
    {

    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void MemManage_Handler(void)
{
  
    while (1)
    {

    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void BusFault_Handler(void)
{

    while (1)
    {

    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void UsageFault_Handler(void)
{

    while (1)
    {

    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void SVC_Handler(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void DebugMon_Handler(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void PendSV_Handler(void)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void SysTick_Handler(void)
{
    HAL_IncTick();
    
    app_timeout_timer_period_elapsed();
    app_time_events_timer_period_elapsed();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void CAN1_RX0_IRQHandler(void)
{    
    HAL_CAN_IRQHandler(&hcan1); 

    CAN_RxHeaderTypeDef header = { 0 };
    uint8_t can_receive_data[8] = { 0 };
    HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &header, can_receive_data);

    can_msg_recv_t rm = 
    { 
    .order_msg_id = header.IDE == CAN_ID_STD ? header.StdId : header.ExtId,
    .data = can_receive_data,
    .data_len = header.DLC,
    .rtr = header.RTR
    };
    // log_warning_g("irq>id=0x%04X", header.StdId);
    canbus_msg_order_handler(rm);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|