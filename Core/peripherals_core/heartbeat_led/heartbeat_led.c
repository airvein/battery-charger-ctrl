#include "settings_core/peripherals/settings_heartbeat_led.h"
#include "peripherals_core/heartbeat_led/inc/heartbeat_led.h"
#include "stm32f1xx_hal.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void heartbeat_led_init(void)
{
    GPIO_InitTypeDef gpio;
    gpio.Pin = HEARTBEAT_LED_PIN;
    gpio.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(HEARTBEAT_LED_PORT, &gpio);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void heartbeat_led_toggle(void)
{
    HAL_GPIO_TogglePin(HEARTBEAT_LED);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
