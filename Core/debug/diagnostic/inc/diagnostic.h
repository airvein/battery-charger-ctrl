#ifndef __DIAGNOSTIC_H
#define __DIAGNOSTIC_H


#include <stdint.h>
#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define DIAGNOSTIC_MAX_NAME_LEN 20

#define DIAG_NO_STATE_HANDLER   NULL

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _diagnostic_s diagnostic_t;

typedef uint8_t (*diagnostic_check_state_cb)(void);

typedef void (*diagnostic_change_state_cb)(diagnostic_t*);

typedef bool (*diagnostic_state_handler_cb)(diagnostic_t*);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _diagnostic_s
{
    char name[DIAGNOSTIC_MAX_NAME_LEN];

    int16_t current_state;
    int16_t prev_state;

    diagnostic_check_state_cb check_state_cb;
    diagnostic_change_state_cb change_state_cb;
    diagnostic_state_handler_cb state_handler_cb;

    bool state_handler_execute;

    bool is_init;

};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

diagnostic_t *diagnostic_create(void);

bool diagnostic_init(diagnostic_t *diagnostic, char *name,
                     diagnostic_check_state_cb   check_state_cb,
                     diagnostic_change_state_cb  change_state_cb,
                     diagnostic_state_handler_cb state_handler_cb);
                     
void diagnostic_handler(diagnostic_t *diagnostic);


#endif /* __DIAGNOSTIC_H */