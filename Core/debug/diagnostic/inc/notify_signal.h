#ifndef __NOTIFY_SIGNAL_H
#define __NOTIFY_SIGNAL_H


#include <stdint.h>
#include <stdbool.h>

#include "stm32f1xx_hal.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef void (notify_signal_on_off_cb)(void);

typedef struct _notify_signal_s
{
    uint8_t quick_signal_num;
    uint8_t slow_signal_num;

    bool is_init;    

} notify_signal_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

notify_signal_t *notify_signal_create(void);

bool notify_signal_init(notify_signal_t *notify, uint8_t quick_sig_num, 
                        uint8_t slow_sig_num);

bool notify_signal_enable(notify_signal_t *notify);

bool notify_signal_disable(notify_signal_t *notify);

bool notify_signal_register_signal_on_cb(notify_signal_on_off_cb sig_on_cb);

bool notify_signal_register_signal_off_cb(notify_signal_on_off_cb sig_off_cb);

void notify_timer_period_elapsed(TIM_HandleTypeDef *htim);


#endif /* __NOTIFY_SIGNAL_H */