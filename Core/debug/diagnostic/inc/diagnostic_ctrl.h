#ifndef __DIAGNOSTIC_CTRL_H
#define __DIAGNOSTIC_CTRL_H


#include "diagnostic.h"

#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _diagnostic_ctrl_s
{
    diagnostic_t **diagnostics_array;
    uint16_t diagnostics_num;

    uint16_t current_diag_id;

    bool is_init;

} diagnostic_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

diagnostic_ctrl_t *diagnostic_ctrl_create(void);

bool diagnostic_ctrl_init(diagnostic_ctrl_t *diag_ctrl, 
                          diagnostic_t **diagnostics_array,
                          uint16_t diagnostics_array_size);

void diagnostic_ctrl_handler(diagnostic_ctrl_t *diag_ctrl);


#endif /* __DIAGNOSTIC_CTRL_H */