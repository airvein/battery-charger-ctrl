#include "inc/diagnostic_ctrl.h"

#include "debug/log_modules/log_modules.h"
#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_diagnostic_ctrl, "%s : %s", \
                                        __FILE__, __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(diagnostic_ctrl_t *diag_ctrl, 
                                       diagnostic_t **diagnostics_array,
                                       uint16_t diagnostics_array_size);

static bool is_diagnostic_ctrl_initialized(diagnostic_ctrl_t *diag_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

diagnostic_ctrl_t *diagnostic_ctrl_create(void)
{
    return (diagnostic_ctrl_t*)malloc(sizeof(diagnostic_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool diagnostic_ctrl_init(diagnostic_ctrl_t *diag_ctrl, 
                          diagnostic_t **diagnostics_array,
                          uint16_t diagnostics_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(diag_ctrl, diagnostics_array,
                                  diagnostics_array_size) == false)
    {
        log_error_m(log_diagnostic_ctrl, "Error diag_ctrl initialize");
        return false;
    }

    diag_ctrl->diagnostics_array = diagnostics_array;
    diag_ctrl->diagnostics_num = diagnostics_array_size;
    diag_ctrl->current_diag_id = 0;
    diag_ctrl->is_init = true;

    log_debug_m(log_diagnostic_ctrl, "Initialize correctly");
    return true;                                  
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_ctrl_handler(diagnostic_ctrl_t *diag_ctrl)
{
    if(is_diagnostic_ctrl_initialized(diag_ctrl) == true)
    {
        diagnostic_t *current_diag = 
            diag_ctrl->diagnostics_array[diag_ctrl->current_diag_id++];

        diagnostic_handler(current_diag);

        if(diag_ctrl->current_diag_id >= diag_ctrl->diagnostics_num)
        {            
            diag_ctrl->current_diag_id = 0;
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(diagnostic_ctrl_t *diag_ctrl, 
                                       diagnostic_t **diagnostics_array,
                                       uint16_t diagnostics_array_size)
{
    return diag_ctrl != NULL && diagnostics_array != NULL 
            && diagnostics_array_size > 0;
}        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_diagnostic_ctrl_initialized(diagnostic_ctrl_t *diag_ctrl)
{
    return diag_ctrl != NULL && diag_ctrl->is_init == true;
}     

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|