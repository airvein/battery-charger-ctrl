#include "inc/diagnostic.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>
#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_diagnostic, "%s : %s", __FILE__, \
                                      __func__))

#define INIT_STATE    -1                               

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_diagnostic_initialized(diagnostic_t *diagnostic);

static bool is_correct_init_parameters(diagnostic_t *diagnostic, char *name,
                                diagnostic_check_state_cb   check_state_cb,
                                diagnostic_change_state_cb  change_state_cb);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

diagnostic_t *diagnostic_create(void)
{
    return (diagnostic_t*)malloc(sizeof(diagnostic_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool diagnostic_init(diagnostic_t *diagnostic, char *name,
                     diagnostic_check_state_cb   check_state_cb,
                     diagnostic_change_state_cb  change_state_cb,
                     diagnostic_state_handler_cb state_handler_cb)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(diagnostic, name, check_state_cb,
                                  change_state_cb) == false)
    {
        log_error_m(log_diagnostic, 
                    "Initialize diagnostic parameters error!");
        return false;
    }

    strcpy(diagnostic->name, name);
    
    diagnostic->check_state_cb = check_state_cb;
    diagnostic->change_state_cb = change_state_cb;
    diagnostic->state_handler_cb = state_handler_cb;

    diagnostic->current_state = 0;
    diagnostic->prev_state = INIT_STATE;

    diagnostic->state_handler_execute = false;
    diagnostic->is_init = true;

    log_info_m(log_diagnostic, 
               "Diagnostic [ %s ] initialized!",
               diagnostic->name);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_handler(diagnostic_t *diagnostic)
{
    if(is_diagnostic_initialized(diagnostic) == false)
    {   
        return ;
    }

    diagnostic->current_state = diagnostic->check_state_cb();
    
    if(diagnostic->current_state != diagnostic->prev_state)
    {
        log_debug_m(log_diagnostic, "Invoke diagnostic change_state_cb");
        diagnostic->change_state_cb(diagnostic);
        diagnostic->prev_state = diagnostic->current_state;

        if(diagnostic->state_handler_cb != NULL)
        {
            log_debug_m(log_diagnostic, 
                        "Start diagnostic state_handler_cb");
            diagnostic->state_handler_execute = true;
        }
    }    

    if(diagnostic->state_handler_execute == true)
    {
        if(diagnostic->state_handler_cb(diagnostic) == true)
        {
            log_debug_m(log_diagnostic, 
                        "Exit diagnostic state_handler_cb");
            diagnostic->state_handler_execute = false;
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(diagnostic_t *diagnostic, char *name,
                                diagnostic_check_state_cb   check_state_cb,
                                diagnostic_change_state_cb  change_state_cb)
{
    return diagnostic != NULL && name != NULL && check_state_cb != NULL 
            && change_state_cb != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_diagnostic_initialized(diagnostic_t *diagnostic)
{
    return diagnostic != NULL && diagnostic->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
