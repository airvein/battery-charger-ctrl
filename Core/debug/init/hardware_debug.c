#include "inc/hardware_debug.h"
#include "debug/hardware/uart/inc/uart_open.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void debug_hardware_init()
{
    uart_hardware_init();
	
	// sd_hardware_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|