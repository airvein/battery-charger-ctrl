#include "log_modules.h"

#include <stdio.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_MODULE_INIT(log_module, name, settings)	\
	{ \
		log_module_init(log_module, \
						name, settings);  \
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static log_module_t sv_log_can_msg = { 0 };
static log_module_t sv_log_can_msg_ctrl = { 0 };

static log_module_t sv_log_procedure = { 0 };
static log_module_t sv_log_procedure_ctrl = { 0 };
static log_module_t sv_log_cmd_procedure = { 0 };
static log_module_t sv_log_cmd_procedure_ctrl = { 0 };

static log_module_t sv_log_diagnostic = { 0 };
static log_module_t sv_log_diagnostic_ctrl = { 0 };
static log_module_t sv_log_notify_sig = { 0 };
static log_module_t sv_log_timeout = { 0 };
static log_module_t sv_log_time_event = { 0 };

static log_module_t sv_log_controller = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

log_module_t *log_procedure = &sv_log_procedure;
log_module_t *log_procedure_ctrl = &sv_log_procedure_ctrl;
log_module_t *log_cmd_procedure = &sv_log_cmd_procedure;
log_module_t *log_cmd_procedure_ctrl = &sv_log_cmd_procedure_ctrl;

log_module_t *log_diagnostic = &sv_log_diagnostic;
log_module_t *log_diagnostic_ctrl = &sv_log_diagnostic_ctrl;
log_module_t *log_notify_sig = &sv_log_notify_sig;
log_module_t *log_timeout = &sv_log_timeout;
log_module_t *log_time_event = &sv_log_time_event;

log_module_t *log_can_msg = &sv_log_can_msg;
log_module_t *log_can_msg_ctrl = &sv_log_can_msg_ctrl;

log_module_t *log_controller = &sv_log_controller;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void init_log_modules()
{
	LOG_MODULE_INIT(log_procedure, "procedure", LOG_PROCEDURE_SETTINGS);

	LOG_MODULE_INIT(log_procedure_ctrl, "procedure_ctrl", 
					LOG_PROC_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_cmd_procedure, "cmd_procedure", 
					LOG_CMD_PROC_SETTINGS);

	LOG_MODULE_INIT(log_cmd_procedure_ctrl, "cmd_procedure_ctrl", 
					LOG_CMD_PROC_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_diagnostic, "diagnostic", LOG_DIAGNOSTIC_SETTINGS);

	LOG_MODULE_INIT(log_diagnostic_ctrl, "diagnostic_ctrl", 
					LOG_DIAGNOSTIC_CTRL_SETTINGS);
	
	LOG_MODULE_INIT(log_notify_sig, "notify", LOG_NOTIFY_SETTINGS);

	LOG_MODULE_INIT(log_controller, LOG_CTRL_NAME, LOG_CONTROLLER_SETTINGS);

	LOG_MODULE_INIT(log_timeout, "timeout", LOG_TIMEOUT_SETTINGS);

	LOG_MODULE_INIT(log_time_event, "time_event", LOG_TIME_EVENT_SETTINGS);

	LOG_MODULE_INIT(log_can_msg, "can_msg", LOG_CAN_MSG_SETTINGS);
	LOG_MODULE_INIT(log_can_msg_ctrl, "can_msg_ctrl", LOG_CAN_MSG_SETTINGS);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|