#include "debug/log/log.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define RED_COLOR_STR       "\x1B[31m"    //error
#define GREEN_COLOR_STR     "\x1B[36m"    //debug
#define YELLOW_COLOR_STR    "\x1B[33m"    //warning
#define CYAN_COLOR_STR      "\x1B[32m"    //info
#define RESET_COLOR_STR     "\x1B[0m"

#define DEBUG_STRING_C      GREEN_COLOR_STR " DEBUG " RESET_COLOR_STR
#define ERROR_STRING_C      RED_COLOR_STR " ERROR " RESET_COLOR_STR
#define WARNING_STRING_C    YELLOW_COLOR_STR "WARNING" RESET_COLOR_STR
#define INFO_STRING_C       CYAN_COLOR_STR " INFO  " RESET_COLOR_STR

#define GLOBAL_LOG_NAME     "GLOBAL"

#define CHECK_GLOBAL_LOG_LVL(log_lvl) \
    {\
        if(is_enable_log_lvl(global_log_level, log_lvl) == false)\
        { return false; }\
    }

#define CHECK_MODULE_LOG_LVL(log_module, log_lvl) \
    {\
        if(log_module == NULL || \
            is_enable_log_lvl(log_module->level, log_lvl) == false)\
        { return false; }\
    }


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t global_log_level = LOG_LVL_ERROR;

static uint32_t *global_timestamp_ptr = NULL;

static int (*global_printf_clbk) (const char *fmt,...) = printf;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_log_lvl(log_lvl_t log_lvl_parameter);

static bool is_enable_log_lvl(uint8_t log_level, log_lvl_t log_lvl_parameter);

static void print_log(char *log_title, char *log_msg, char *module_name);

static bool log_prepare(char *lvl_msg, char *module_name, const char *log_msg,
						va_list argp);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_register_printf_clbk(int(*printf_clbk)(const char *fmt,...))
{
    if(printf_clbk != NULL)
    {
        global_printf_clbk = printf_clbk;
        return true;
    }
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_set_timestamp_var_ptr(uint32_t *timestamp_ptr)
{
    if(timestamp_ptr != NULL)
    {
        global_timestamp_ptr = timestamp_ptr;
        return true;
    }
    return false; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void log_clear_timestamp_var_ptr(void)
{
    global_timestamp_ptr = NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_set_global_log_level(log_lvl_t log_lvl)
{
    if(is_correct_log_lvl(log_lvl) == true)
    {    	
        global_log_level = log_lvl;    
        return true; 
    }
    else
    {
        global_log_level = LOG_LVL_ERROR;
        return false; 
    }          
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t log_get_global_log_level(void)
{
    return global_log_level;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_debug_g(const char *log_msg, ...)
{
    CHECK_GLOBAL_LOG_LVL(LOG_LVL_DEBUG);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(DEBUG_STRING_C, GLOBAL_LOG_NAME,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_info_g(const char *log_msg, ...)
{
    CHECK_GLOBAL_LOG_LVL(LOG_LVL_INFO);    
    va_list argp;
	va_start(argp, log_msg);
	
	bool retval = log_prepare(INFO_STRING_C, GLOBAL_LOG_NAME,
                              log_msg, argp);
	
	va_end(argp);
	
    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_warning_g(const char *log_msg, ...)
{
    CHECK_GLOBAL_LOG_LVL(LOG_LVL_WARNING);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(WARNING_STRING_C, GLOBAL_LOG_NAME,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_error_g(const char *log_msg, ...)
{
    CHECK_GLOBAL_LOG_LVL(LOG_LVL_ERROR);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(ERROR_STRING_C, GLOBAL_LOG_NAME,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

log_module_t *log_module_create()
{
    return (log_module_t*)malloc(sizeof(log_module_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_module_init(log_module_t *log_module, char *module_name, 
                     log_lvl_t log_lvl)
{
    if(log_module == NULL)
    {
        return false;
    }

    if(is_correct_log_lvl(log_lvl) == true)
    {
        log_module->level = log_lvl;
    }
    else
    {
        log_module->level = LOG_LVL_ERROR;
        return false;
    }

    memcpy(log_module->name, module_name, MODULE_NAME_LEN);
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_debug_m(log_module_t *log_module, const char *log_msg, ...)
{
    CHECK_MODULE_LOG_LVL(log_module, LOG_LVL_DEBUG);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(DEBUG_STRING_C, log_module->name,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_info_m(log_module_t *log_module, const char *log_msg, ...)
{
    CHECK_MODULE_LOG_LVL(log_module, LOG_LVL_INFO);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(INFO_STRING_C, log_module->name,
                              log_msg, argp);
	va_end(argp);

    return retval; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_warning_m(log_module_t *log_module, const char *log_msg, ...)
{
    CHECK_MODULE_LOG_LVL(log_module, LOG_LVL_WARNING);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(WARNING_STRING_C, log_module->name,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int log_error_m(log_module_t *log_module, const char *log_msg, ...)
{
    CHECK_MODULE_LOG_LVL(log_module, LOG_LVL_ERROR);
    va_list argp;
	va_start(argp, log_msg);
	bool retval = log_prepare(ERROR_STRING_C, log_module->name,
                              log_msg, argp);
	va_end(argp);

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void print_log(char *log_title, char *log_msg, char *module_name)
{
    if(global_timestamp_ptr == NULL)
    {
        global_printf_clbk("%s >> %s\r\n", log_title, log_msg);       
    }
    else
    {
        global_printf_clbk("(%ld) %s >> %s\r\n", *global_timestamp_ptr,
                                                log_title, log_msg);
    }   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_log_lvl(log_lvl_t log_lvl_parameter)
{
    return ((log_lvl_parameter >= LOG_LVL_DISABLE)
            && (log_lvl_parameter <= LOG_LVL_ALL));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_enable_log_lvl(uint8_t log_level, log_lvl_t log_lvl_parameter)
{
    if(log_level == LOG_LVL_DISABLE)
    {
        return false;
    }

    return log_level & log_lvl_parameter;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool log_prepare(char *lvl_msg, char *module_name, const char *log_msg,
						va_list argp)
{
    char string[255];
    char log_title[70];
    sprintf(log_title, "[%s / %s]", lvl_msg, module_name);

    if(0 < vsprintf(string,log_msg,argp))
    {
    	print_log(log_title, string, module_name);
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
