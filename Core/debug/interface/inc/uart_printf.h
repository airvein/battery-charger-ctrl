/**
  ******************************************************************************
  * @file           : uart_printf.h
  * @brief          : Header file for uart_printf
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#ifndef __UART_PRINTF_H
#define __UART_PRINTF_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "stm32f1xx_hal.h"

int uart_printf(const char *fmt, ...);

#endif /* __UART_PRINTF_H */
