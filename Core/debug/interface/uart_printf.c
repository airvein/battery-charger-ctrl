/**
  ******************************************************************************
  * @file           : uart_printf.c
  * @brief          : Printf functions via serial (USART)
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#include "inc/uart_printf.h"
#include <string.h>
#include <stdbool.h>



#define SERVICE_DEBUG

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void usart2_send_char(char c)
{
	while(!(USART2->SR & USART_SR_TXE))
	{ ; }
	USART2->DR = c;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void usart2_send_string(char *string, bool new_line)
{
	while(*string != '\0')
	{
		usart2_send_char(*string);
		string++;
	}
    if(new_line == true)
    {
        usart2_send_char('\r');
        usart2_send_char('\n');
    }
}

static void vprint(const char *fmt, va_list argp)
{
    char string[255];

    if(0 < vsprintf(string,fmt,argp))
    {
        usart2_send_string(string, 0);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int uart_printf(const char *fmt, ...)
{
	#ifdef SERVICE_DEBUG
		va_list argp;
		va_start(argp, fmt);
		vprint(fmt, argp);
		va_end(argp);
	#endif
	return 1;
}

