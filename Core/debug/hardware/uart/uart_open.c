#include "settings_core/communication/settings_uart.h"
#include "debug/hardware/uart/inc/uart_open.h"

#include "debug/log/log.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_gpio_init();
void uart_peripherials_init();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_hardware_init()
{
	uart_gpio_init();
	uart_peripherials_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_gpio_init()
{
  	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN_Msk;
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
	//tx pp / rx input pup float
	
	/* TX - pa2 alternat push-pull */
	GPIOA->CRL &= ~(GPIO_CRL_CNF2);
	GPIOA->CRL |= GPIO_CRL_CNF2_1;
	/* output mode 50 mhz */
	GPIOA->CRL |= GPIO_CRL_MODE2;

	/* RX - PA3 input pullup */
	GPIOA->CRL &= ~(GPIO_CRL_CNF3);
	GPIOA->CRL |= GPIO_CRL_CNF3_1;
	/* pull up */
	GPIOA->ODR |= GPIO_PIN_3;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_peripherials_init()
{
	USART2->BRR = 36000000/115200;
	// USART2->BRR = 36000000/115200;
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
