#ifndef __TIMEOUT_H
#define __TIMEOUT_H

#include <stdint.h>
#include <stdbool.h>

#include "stm32f1xx_hal.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _timeout_s timeout_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _timeout_state_e
{
    TIMEOUT_STATE_IDLE,
    TIMEOUT_STATE_RUNNING,
    TIMEOUT_STATE_TRIGGERED

} timeout_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _timeout_s
{
    uint16_t id;

    uint32_t set_interval_multiple;
    uint32_t cnt_interval_multiple;

    timeout_state_t state;

    bool is_init;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

timeout_t *timeout_create(void);

bool timeout_init(timeout_t *tout, uint16_t id, uint32_t interval_multiple);

bool timeout_start(timeout_t *tout);

bool timeout_stop(timeout_t *tout);

bool timeout_reset(timeout_t *tout);

timeout_state_t timeout_get_state(timeout_t *tout);

bool timeout_register_timeouts_array(timeout_t **tout_array, 
                                     uint16_t array_size);

// void timeout_initialize_timer(void);

void timeout_handler(void);

void timeout_timer_period_elapsed(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __TIMEOUT_H */
