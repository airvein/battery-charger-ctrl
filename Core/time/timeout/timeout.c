#include "inc/timeout.h"

#include "debug/log_modules/log_modules.h"
#include "settings_core/global_settings_timer.h"

#include <stdlib.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_timeout, "%s : %s", __FILE__, \
                                      __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static TIM_HandleTypeDef timeout_timer;
// static bool timer_init_flag = false;
// static bool timer_start_flag = false;

static timeout_t **timeout_array = NULL;
static uint16_t timeout_array_size = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(timeout_t *tout, uint32_t time_ms);

static bool is_timeout_init(timeout_t *tout);

static bool is_timeouts_array_init(void);

// static void start_timeout_timer(void);

static void single_timeout_handler(timeout_t *tout);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

timeout_t *timeout_create(void)
{
    return (timeout_t*)malloc(sizeof(timeout_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool timeout_init(timeout_t *tout, uint16_t id, uint32_t interval_multiple)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(tout, interval_multiple) == false)
    {
        log_error_m(log_timeout, "Initilize timeout [ %d ] parameters error",
                    id);
        return false;
    }

    tout->id = id;    
    tout->set_interval_multiple = interval_multiple;
    tout->cnt_interval_multiple = 0;    
    tout->state = TIMEOUT_STATE_IDLE;
    tout->is_init = true;

    // log_debug_m(log_timeout, "Timeout [ %d ] initialized with time [ %d ] ms "
    //             "succesfull!", 
    //             tout->id, 
    //             tout->set_interval_multiple * TIMEOUT_TIM_INTERVAL_MS);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool timeout_start(timeout_t *tout)
{
    if(is_timeout_init(tout) == false)
    {
        return false;
    }

    if(tout->state == TIMEOUT_STATE_IDLE)
    {
        tout->cnt_interval_multiple = tout->set_interval_multiple;
        tout->state = TIMEOUT_STATE_RUNNING;

        log_debug_m(log_timeout, "Start running timeout [ %d ]",
                    tout->id);
        return true;
    }
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool timeout_stop(timeout_t *tout)
{
    if(is_timeout_init(tout) == false)
    {
        return false;
    }

    if(tout->state == TIMEOUT_STATE_RUNNING)
    {
        tout->state = TIMEOUT_STATE_IDLE;
        tout->cnt_interval_multiple = 0;
        
        log_debug_m(log_timeout, "Stop timeout [ %d ]",
                    tout->id);
        return true;
    }
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool timeout_reset(timeout_t *tout)
{
    if(is_timeout_init(tout) == false)
    {
        return false;
    }

    // if(tout->state == TIMEOUT_STATE_TRIGGERED)
    // {
        tout->state = TIMEOUT_STATE_IDLE;
        tout->cnt_interval_multiple = 0;

        log_debug_m(log_timeout, "Reset triggered timeout [ %d ]",
                    tout->id);
        return true;
    // }
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

timeout_state_t timeout_get_state(timeout_t *tout)
{
    if(is_timeout_init(tout) == false)
    {
        log_error_m(log_timeout, "Try get state null timeout object!");
        return TIMEOUT_STATE_TRIGGERED;
    }

    return tout->state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool timeout_register_timeouts_array(timeout_t **tout_array, 
                                     uint16_t array_size)
{
    LOG_DEBUG_FUNC();
    if(tout_array == NULL || array_size == 0)
    {
        log_error_m(log_timeout, "Register empty timeout array");
        return false;
    }

    timeout_array = tout_array;
    timeout_array_size = array_size;

    log_debug_m(log_timeout, "Register timeout array succesfull!");

    return true;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// void timeout_initialize_timer(void)
// {
//     LOG_DEBUG_FUNC();
// 	TIMEOUT_TIM_CLK_ENABLE();
// 	timeout_timer.Instance = TIMEOUT_TIM_INSTANCE;
// 	timeout_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
// 	timeout_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
// 	timeout_timer.Init.Prescaler = 20000-1;
// 	timeout_timer.Init.Period = TIMEOUT_TIM_INTERVAL_MS-1;
// 	timeout_timer.Init.ClockDivision = 0;

// 	HAL_TIM_Base_Init(&timeout_timer);
// 	HAL_NVIC_SetPriority(TIMEOUT_TIM_IRQn, 4, 0);

//     start_timeout_timer();

//     timer_init_flag = true;
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void timeout_handler(void)
{   
    static int tout_id = 0;

    for(tout_id = 0; tout_id < timeout_array_size; tout_id++)
    {
        single_timeout_handler(timeout_array[tout_id]);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void timeout_timer_period_elapsed(void)
{
	timeout_handler();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(timeout_t *tout, 
                                       uint32_t interval_multiple)
{
    return tout != NULL && interval_multiple > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_timeout_init(timeout_t *tout)
{
    return tout != NULL && tout->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/* TODO: todo_text */
// void TIMEOUT_TIM_IRQHandler()
// {
// 	HAL_TIM_IRQHandler(&timeout_timer);
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_timeouts_array_init(void)
{
    bool retVal = timeout_array != NULL && timeout_array_size > 0;

    if(!retVal)
    {
        log_warning_m(log_timeout, "Timeout array is not initiliazed!");
        return false;
    }

    for(int _tout_id = 0; _tout_id < timeout_array_size; _tout_id++)
    {
        if(is_timeout_init(timeout_array[_tout_id]) == false)
        {
            log_error_m(log_timeout, 
                        "Timeout object in timeout array with array id"
                        " [ %d ] is NULL",
                        _tout_id);
            return false;
        }
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static void start_timeout_timer(void)
// {
//     if(timer_start_flag)
//     {
//         return ;
//     }

//     if(is_timeouts_array_init() == true)
//     {
//         __HAL_TIM_CLEAR_IT(&timeout_timer, TIM_IT_UPDATE);
//         HAL_NVIC_EnableIRQ(TIMEOUT_TIM_IRQn);
//         HAL_TIM_Base_Start_IT(&timeout_timer);
        
//         timer_start_flag = true;

//         log_debug_m(log_timeout, "Start timeout timer!");
//     }
//     else
//     {
//         log_warning_m(log_timeout, "Can not start timeout timer "
//                                     "(not initialized timeout array!)");
//     }
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void single_timeout_handler(timeout_t *tout)
{
    if(tout->state == TIMEOUT_STATE_RUNNING)
    {
        if(tout->cnt_interval_multiple > 0)
        {
            // log_debug_m(log_timeout, "%d", tout->cnt_interval_multiple);
            tout->cnt_interval_multiple--;
        }
        else
        {
            tout->state = TIMEOUT_STATE_TRIGGERED;
            log_debug_m(log_timeout, 
                        "Timeout [ %d ] TRIGGERED!",
                        tout->id);
        }        
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|