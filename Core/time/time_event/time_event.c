#include "inc/time_event.h"

#include "debug/log_modules/log_modules.h"
#include "settings_core/global_settings_timer.h"

#include <stdlib.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_time_event, "%s : %s", __FILE__, \
                                      __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static TIM_HandleTypeDef time_event_timer;
// static bool timer_init_flag = false;
// static bool timer_start_flag = false;

static time_event_t **time_event_array = NULL;
static uint16_t time_event_array_size = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(time_event_t *timevt, 
                                       uint32_t interval_multiple, 
                                       time_event_cb event_callback);

static bool is_time_event_init(time_event_t *tevt);

static bool is_time_event_array_init(void);

// static void start_time_event_timer(void);

static void single_time_event_timer_handler(time_event_t *tevt);

static void single_time_event_exec_handler(time_event_t *tevt);

static bool is_time_event_triggered(time_event_t *tevt);

static void reload_time_event(time_event_t *tevt);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

time_event_t *time_event_create(void)
{
    return (time_event_t*)malloc(sizeof(time_event_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool time_event_init(time_event_t *tevt, uint16_t id, 
                     uint32_t interval_multiple, time_event_cb event_callback)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(tevt, interval_multiple, 
                                  event_callback) == false)
    {
        log_error_m(log_time_event, 
                    "Initialize parameters error for [ %d ] event", id);
        return false;
    }

    tevt->id = id;

    tevt->set_interval_multiple = interval_multiple;
    tevt->cnt_interval_multiple = interval_multiple;

    tevt->state = TIME_EVT_STATE_IDLE;

    tevt->event_callback = event_callback;

    tevt->is_init = true;

    log_debug_m(log_time_event, 
                "Initialize time_event with id [ %d ] succesfull!",
                tevt->id);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool time_event_register_events_array(time_event_t **tevt_array, 
                                     uint16_t array_size)
{
    if(tevt_array == NULL || array_size == 0)
    {
        log_error_m(log_time_event, "Register empty time_event array!");
        return false;
    }

    time_event_array = tevt_array;
    time_event_array_size = array_size;

    log_debug_m(log_time_event, "Register time_events array succesfull!");

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// void time_event_initialize_timer(void)
// {
//     LOG_DEBUG_FUNC();
// 	TIME_EVENT_TIM_CLK_ENABLE();
// 	time_event_timer.Instance = TIME_EVENT_TIM_INSTANCE;
// 	time_event_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
// 	time_event_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
// 	time_event_timer.Init.Prescaler = 20000-1;
// 	time_event_timer.Init.Period = TIME_EVENT_TIM_INTERVAL_MS-1;
// 	time_event_timer.Init.ClockDivision = 0;

// 	HAL_TIM_Base_Init(&time_event_timer);
// 	HAL_NVIC_SetPriority(TIME_EVENT_TIM_IRQn, 4, 0);

//     start_time_event_timer();

//     timer_init_flag = true;
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void time_event_timer_period_elapsed(void)
{
    time_event_timer_handler();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void time_event_timer_handler(void)
{
    static int tevtt_id = 0;
    for(tevtt_id = 0; tevtt_id < time_event_array_size; tevtt_id++)
    {
        single_time_event_timer_handler(time_event_array[tevtt_id]);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void time_event_exec_handler(void)
{
    static int tevte_id = 0;
    for(tevte_id = 0; tevte_id < time_event_array_size; tevte_id++)
    {
        single_time_event_exec_handler(time_event_array[tevte_id]);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/* TODO: todo_text */
// void TIME_EVENT_TIM_IRQHandler()
// {
// 	HAL_TIM_IRQHandler(&time_event_timer);
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(time_event_t *tevt, 
                                       uint32_t interval_multiple, 
                                       time_event_cb event_callback)
{
    return tevt != NULL && interval_multiple > 0 && event_callback != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_time_event_init(time_event_t *tevt)
{
    return tevt != NULL && tevt->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_time_event_array_init(void)
{
    bool retVal = time_event_array != NULL && time_event_array_size > 0;

    if(!retVal)
    {
        log_warning_m(log_time_event, "Time event array is not initiliazed!");
        return false;
    }

    for(int _tevt_id = 0; _tevt_id < time_event_array_size; _tevt_id++)
    {
        if(is_time_event_init(time_event_array[_tevt_id]) == false)
        {
            log_error_m(log_time_event, 
                        "Time event object in time event array with array id"
                        " [ %d ] is NULL",
                        _tevt_id);
            return false;
        }
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static void start_time_event_timer(void)
// {
//     if(timer_start_flag)
//     {
//         return ;
//     }

//     if(is_time_event_array_init() == true)
//     {
//         __HAL_TIM_CLEAR_IT(&time_event_timer, TIM_IT_UPDATE);
//         HAL_NVIC_EnableIRQ(TIME_EVENT_TIM_IRQn);
//         HAL_TIM_Base_Start_IT(&time_event_timer);
        
//         timer_start_flag = true;

//         log_debug_m(log_time_event, "Start time event timer!");
//     }
//     else
//     {
//         log_warning_m(log_time_event, "Can not start time event timer "
//                                     "(not initialized time event array!)");
//     }
// }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void single_time_event_timer_handler(time_event_t *tevt)
{
    if(is_time_event_triggered(tevt) == false)
    {
        // log_debug_m(log_time_event, "%d", tevt->cnt_interval_multiple);
        tevt->cnt_interval_multiple--;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void single_time_event_exec_handler(time_event_t *tevt)
{
    if(is_time_event_triggered(tevt) == true)
    {
        log_debug_m(log_time_event, "Time event [ %d ] executing!",
                    tevt->id);

        if(tevt->event_callback() == TIME_EVT_RET_DONE)
        { 
            reload_time_event(tevt);
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_time_event_triggered(time_event_t *tevt)
{
    return tevt->cnt_interval_multiple == 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void reload_time_event(time_event_t *tevt)
{
    tevt->cnt_interval_multiple = tevt->set_interval_multiple;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|