#ifndef __TIME_EVENT_H
#define __TIME_EVENT_H

#include <stdint.h>
#include <stdbool.h>

#include "stm32f1xx_hal.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _time_event_s time_event_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _time_event_ret_e
{
    TIME_EVT_RET_PENDING,
    TIME_EVT_RET_DONE

} time_evt_ret_t;

typedef time_evt_ret_t (*time_event_cb) (void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _time_event_state_e
{
    TIME_EVT_STATE_IDLE,
    TIME_EVT_STATE_EXEC

} time_event_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _time_event_s
{
    uint16_t id;

    uint32_t set_interval_multiple;
    uint32_t cnt_interval_multiple;

    time_event_state_t state;

    time_event_cb event_callback;

    bool is_init;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

time_event_t *time_event_create(void);

bool time_event_init(time_event_t *tevt, uint16_t id, 
                     uint32_t interval_multiple, time_event_cb event_callback);

bool time_event_register_events_array(time_event_t **tevt_array, 
                                     uint16_t array_size);

// void time_event_initialize_timer(void);

void time_event_timer_period_elapsed(void);

void time_event_timer_handler(void);

void time_event_exec_handler(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __TIME_EVENT_H */
