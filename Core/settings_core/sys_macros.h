#ifndef __SYS_MACROS_H
#define __SYS_MACROS_H

#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define __CHECK_EXPR_AND_RET(expr, expr_bool_state, retval) \
    {\
        if((expr) == expr_bool_state)\
        {\
            return retval;\
        }\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_FALSE_AND_RET(expr, retval) \
    {\
        __CHECK_EXPR_AND_RET(expr, false, retval);\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_AND_RET(expr, retval) \
    {\
        __CHECK_EXPR_AND_RET(expr, true, retval);\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_NULL_AND_RET(ptr, retval) \
    {\
        CHECK_AND_RET(ptr == NULL, retval);\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define EXIT_FUNC_EXPR(expr) \
    {\
        CHECK_AND_RET(expr, ; );\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_AND_INF_LOOP_MSG(expr, log_cb, msg, ...)\
    {\
        if((expr) == true)\
        {\
            log_cb(msg, ##__VA_ARGS__);\
            log_cb("err in [ %s : %s()  line: %ld ]", \
                __FILE__, __func__, __LINE__-2); \
            log_cb("infinite loop...");\
            while(1);\
        }\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_AND_HANDLE(expr, handler_cb) \
    {\
        if((expr) == true)\
        {\
            handler_cb();\
        }\
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CHECK_AND_HANDLE_MSG(expr, handler_cb, log_cb, msg, ...) \
    {\
        if((expr) == true)\
        {\
            log_cb(msg, ##__VA_ARGS__);\
            handler_cb();\
        }\
    }
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ARRAY_SIZE(array)   (sizeof(array)/sizeof(array[0]))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SYS_MACROS_H */