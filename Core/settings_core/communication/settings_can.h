#ifndef __SETTINGS_CAN_H
#define __SETTINGS_CAN_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CAN_PORT                        GPIO_CAN_PORT
#define CAN_RX_PIN                      GPIO_CAN_RX_PIN
#define CAN_TX_PIN                      GPIO_CAN_TX_PIN

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_CAN_H */