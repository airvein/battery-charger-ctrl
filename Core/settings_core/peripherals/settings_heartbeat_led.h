#ifndef __SETTINGS_HEARTBEAT_LED_H
#define __SETTINGS_HEARTBEAT_LED_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define HEARTBEAT_LED_PORT             GPIO_HEARTBEAT_LED_PORT
#define HEARTBEAT_LED_PIN              GPIO_HEARTBEAT_LED_PIN
#define HEARTBEAT_LED                  GPIO_HEARTBEAT_LED

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_HEARTBEAT_LED_H */