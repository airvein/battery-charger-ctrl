#ifndef __GLOBAL_SETTINGS_TIMER_H
#define __GLOBAL_SETTINGS_TIMER_H

#include "stm32f1xx_hal.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define NOTIFY_TIM_CLK_ENABLE()         __TIM2_CLK_ENABLE();
#define NOTIFY_TIM_INSTANCE             TIM2
#define NOTIFY_TIM_IRQn                 TIM2_IRQn
#define NOTIFY_TIM_IRQHandler           TIM2_IRQHandler

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define SYSTICK_CLK_FREQ_MHZ            72000000

#define SYSTICK_DIV_100_MS              10
#define SYSTICK_DIV_10_MS               100
#define SYSTICK_DIV_1_MS                1000

#define SYSTICK_DIV                     SYSTICK_DIV_100_MS
#define SYSTICK_IRQ_TICK                SYSTICK_CLK_FREQ_MHZ/SYSTICK_DIV

#define SYSTICK_INTERVAL_SEC(sec)       ((sec) * (SYSTICK_DIV))

#define __SYSTICK_DISABLE \
    SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk)
#define __SYSTICK_ENABLE \
    SysTick->CTRL |= (SysTick_CTRL_ENABLE_Msk)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __GLOBAL_SETTINGS_TIMER_H */