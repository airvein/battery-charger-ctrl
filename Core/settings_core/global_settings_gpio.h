#ifndef __GLOBAL_SETTINGS_GPIO_H
#define __GLOBAL_SETTINGS_GPIO_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CTRL_BOARD

// ETH
#define GPIO_ETH_PORT_A                 GPIOA
#define GPIO_ETH_REF_CLK_PIN            GPIO_PIN_1
#define GPIO_ETH_MDIO_PIN               GPIO_PIN_2
#define GPIO_ETH_CRS_DV_PIN             GPIO_PIN_7
#define GPIO_ETH_PORT_B                 GPIOB
#define GPIO_ETH_TXD1_PIN               GPIO_PIN_13
#define GPIO_ETH_PORT_C                 GPIOC
#define GPIO_ETH_MDC_PIN                GPIO_PIN_1
#define GPIO_ETH_RXD0_PIN               GPIO_PIN_4
#define GPIO_ETH_RXD1_PIN               GPIO_PIN_5
#define GPIO_ETH_PORT_G                 GPIOG
#define GPIO_ETH_TX_EN_PIN              GPIO_PIN_11
#define GPIO_ETH_TXD0_PIN               GPIO_PIN_13

// USB
#define G_USB_PowerSwitchOn_GPIO_Port   GPIOG
#define G_USB_PowerSwitchOn_Pin         GPIO_PIN_6
#define G_USB_OverCurrent_GPIO_Port     GPIOG
#define G_USB_OverCurrent_Pin           GPIO_PIN_7
#define G_USB_SOF_GPIO_Port             GPIOA
#define G_USB_SOF_Pin                   GPIO_PIN_8
#define G_USB_VBUS_GPIO_Port            GPIOA
#define G_USB_VBUS_Pin                  GPIO_PIN_9
#define G_USB_ID_GPIO_Port              GPIOA
#define G_USB_ID_Pin                    GPIO_PIN_10
#define G_USB_DM_GPIO_Port              GPIOA
#define G_USB_DM_Pin                    GPIO_PIN_11
#define G_USB_DP_GPIO_Port              GPIOA
#define G_USB_DP_Pin                    GPIO_PIN_12

// CAN
#define GPIO_CAN_PORT                   GPIOB
#define GPIO_CAN_RX_PIN                 GPIO_PIN_8
#define GPIO_CAN_TX_PIN                 GPIO_PIN_9

// UART
#define GPIO_USART3_PORT	            GPIOD
#define GPIO_USART3_TX_PIN		        GPIO_PIN_8
#define GPIO_USART3_RX_PIN		        GPIO_PIN_9

// Heartbeat led

#ifdef CTRL_BOARD
#define GPIO_HEARTBEAT_LED_PORT         GPIOA
#define GPIO_HEARTBEAT_LED_PIN          GPIO_PIN_3
#define GPIO_HEARTBEAT_LED              GPIO_HEARTBEAT_LED_PORT,GPIO_HEARTBEAT_LED_PIN
#endif

#ifdef TEST_BOARD
#define GPIO_HEARTBEAT_LED_PORT         GPIOB
#define GPIO_HEARTBEAT_LED_PIN          GPIO_PIN_7
#define GPIO_HEARTBEAT_LED              GPIO_HEARTBEAT_LED_PORT,GPIO_HEARTBEAT_LED_PIN
#endif

// SPI1 (for PLC communication)
#define GPIO_SPI1_SCK_PORT              GPIOA
#define GPIO_SPI1_SCK_PIN               GPIO_PIN_5
#define GPIO_SPI1_SCK                   GPIO_SPI1_SCK_PORT,GPIO_SPI1_SCK_PIN

#define GPIO_SPI1_MISO_PORT             GPIOA
#define GPIO_SPI1_MISO_PIN              GPIO_PIN_6
#define GPIO_SPI1_MISO                  GPIO_SPI1_MISO_PORT,GPIO_SPI1_MISO_PIN

#define GPIO_SPI1_MOSI_PORT             GPIOD
#define GPIO_SPI1_MOSI_PIN              GPIO_PIN_7
#define GPIO_SPI1_MOSI                  GPIO_SPI1_MOSI_PORT,GPIO_SPI1_MOSI_PIN

//PLC module
#define GPIO_PLC_CURRENT_CS_PORT 	    GPIOD
#define GPIO_PLC_CURRENT_CS_PIN 	    GPIO_PIN_15
#define GPIO_PLC_CURRENT_CS 	        GPIO_PLC_CURRENT_CS_PORT,GPIO_PLC_CURRENT_CS_PIN

#define GPIO_PLC_RELAY_CS_PORT 	        GPIOD
#define GPIO_PLC_RELAY_CS_PIN  	        GPIO_PIN_14
#define GPIO_PLC_RELAY_CS               GPIO_PLC_RELAY_CS_PORT,GPIO_PLC_RELAY_CS_PIN

#define GPIO_PLC_RELAY_EN_PORT          GPIOE
#define GPIO_PLC_RELAY_EN_PIN           GPIO_PIN_9
#define GPIO_PLC_RELAY_EN               GPIO_PLC_RELAY_EN_PORT,GPIO_PLC_RELAY_EN_PIN

// SD card
#define GPIO_SDMCC1_D0_CK_PORT          GPIOC
#define GPIO_SDMCC1_D0_PIN              GPIO_PIN_8
#define GPIO_SDMCC1_CK_PIN              GPIO_PIN_12
#define GPIO_SDMCC1_CMD_PORT            GPIOD
#define GPIO_SDMCC1_CMD_PIN             GPIO_PIN_2
#define GPIO_SDMCC1_DETECT_PORT         GPIOB
#define GPIO_SDMCC1_DETECT_PIN          GPIO_PIN_11



#endif /* __GLOBAL_SETTINGS_GPIO_H */